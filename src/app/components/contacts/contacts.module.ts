import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';

import { IStoreContactsComponent } from './contacts.component';

const route: Route = {
  path: '',
  component: IStoreContactsComponent,
};

@NgModule({
  declarations: [IStoreContactsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([route]),
  ],
  exports: [IStoreContactsComponent],
})
export class IStoreContactsModule { }
