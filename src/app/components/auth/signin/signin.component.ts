import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError, of, takeUntil, tap } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../../../enums';
import { IStoreUserInterface } from '../../../interfaces';
import { USER_LIST_MOCK } from '../../../mocks';
import { IStoreAuthService, IStoreDestroyService, IStoreUserService } from '../../../services';
import { PhoneNumber } from '../../../utils';

const signFormControlErrors: { [key: string]: string } = {
  incorrectPhoneNumber: 'Incorrect phone number',
  invalidPassword: 'Invalid password',
  required: '{fieldName} is required',
  userNotFound: 'User not found',
  unknown: 'Unknown error',
};

@Component({
  selector: 'istore-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  providers: [IStoreDestroyService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IStoreSignInComponent {
  public readonly userList = USER_LIST_MOCK;
  public readonly signForm = this.formBuilder.group({
    password: [''],
    phoneNumber: [''],
  });

  public readonly formatPhoneNumber$ = this.signForm.controls.phoneNumber
    .valueChanges
    .pipe(
      tap(phone => {
        this.signForm.controls.phoneNumber.patchValue(PhoneNumber.setFormat(phone), { emitEvent: false });
      }));

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly iStoreAuthService: IStoreAuthService,
    private readonly destroy$: IStoreDestroyService,
    private readonly iStoreUserService: IStoreUserService,
  ) { }

  public fillFields(user: Partial<IStoreUserInterface>): void {
    this.signForm.patchValue(user);
    this.signForm.markAsDirty();
  }

  public getControlError(errors: ValidationErrors | null, fieldName: string): string {
    const lastError = Object.keys(errors ?? { unknown: true }).pop();

    return signFormControlErrors[lastError ?? 'unknown'].replace('{fieldName}', fieldName);
  }

  public signIn(): void {
    const { password, phoneNumber } = this.signForm.controls;
    password.setValidators([Validators.required]);
    password.updateValueAndValidity();
    phoneNumber.setValidators([Validators.required, PhoneNumber.validator()]);
    phoneNumber.updateValueAndValidity();
    const { valid, value, dirty } = this.signForm;

    if (dirty && valid && value.password && value.phoneNumber) {
      this.iStoreAuthService
        .singIn({
          password: value.password,
          phoneNumber: PhoneNumber.cleanFormat(value.phoneNumber),
        })
        .pipe(
          tap(user => {
            this.iStoreUserService.currentUser$.next(user);
            this.router.navigate([IStoreRoutingPathsEnum.Account]);
          }),
          catchError(error => {
            error?.error?.invalidPassword && password.setErrors({ invalidPassword: true });
            error?.error?.userNotFound && phoneNumber.setErrors({ userNotFound: true });
            this.cdr.detectChanges();

            return of(error);
          }),
          takeUntil(this.destroy$))
        .subscribe();
    }
  }
}
