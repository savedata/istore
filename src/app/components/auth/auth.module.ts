import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';

import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreButtonModule, IStoreDigitsFieldModule, IStoreInputFieldModule, IStoreSelectModule } from '../ui';

import { IStoreAuthComponent } from './auth.component';
import { IStoreForgotPasswordComponent } from './forgot-password';
import { IStoreSignInComponent } from './signin';
import { IStoreSignUpComponent } from './signup';

const route: Route = {
  path: '',
  component: IStoreAuthComponent,
  children: [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: IStoreRoutingPathsEnum.SignIn,
    },
    {
      path: IStoreRoutingPathsEnum.ForgotPassword,
      component: IStoreForgotPasswordComponent,
    },
    {
      path: IStoreRoutingPathsEnum.SignIn,
      component: IStoreSignInComponent,
    },
    {
      path: IStoreRoutingPathsEnum.SignUp,
      component: IStoreSignUpComponent,
    },
  ],
};

@NgModule({
  declarations: [
    IStoreAuthComponent,
    IStoreForgotPasswordComponent,
    IStoreSignInComponent,
    IStoreSignUpComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([route]),
    IStoreButtonModule,
    IStoreDigitsFieldModule,
    IStoreInputFieldModule,
    IStoreSelectModule,
  ],
})
export class IStoreAuthModule { }
