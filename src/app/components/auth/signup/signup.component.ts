import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject, catchError, filter, map, of, switchMap, takeUntil, tap } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../../../enums';
import { IStoreUserInterface } from '../../../interfaces';
import { USER_LIST_MOCK } from '../../../mocks';
import { IStoreAuthService, IStoreDestroyService, IStoreUserService } from '../../../services';
import { PhoneNumber } from '../../../utils';

const signFormControlErrors: { [key: string]: string } = {
  incorrectPhoneNumber: 'Incorrect phone number',
  phoneNotUnique: 'User is already registered',
  required: 'Phone number is required',
  unableToConfirm: 'Unable to confirm phone number <br>Confirmation code is not correct <br>Please reenter the code',
  unknown: 'Unknown error',
};

@Component({
  selector: 'istore-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [IStoreDestroyService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IStoreSignUpComponent {
  public readonly codeLength = 4;
  public readonly userList = USER_LIST_MOCK;
  public readonly signForm = this.formBuilder.group({
    confirmationCode: [''],
    phoneNumber: [''],
  });

  public readonly checkCode$ = this.signForm.controls.confirmationCode
    .valueChanges
    .pipe(
      filter(confirmationCode => confirmationCode !== null && confirmationCode.length === this.codeLength),
      switchMap(confirmationCode => this.confirmPhoneNumber(confirmationCode)));

  public readonly formatPhoneNumber$ = this.signForm.controls.phoneNumber
    .valueChanges
    .pipe(
      tap(phone => {
        this.signForm.controls.phoneNumber.patchValue(PhoneNumber.setFormat(phone), { emitEvent: false });
      }));

  public readonly isPhoneForm$ = new BehaviorSubject<boolean>(true);

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly formBuilder: FormBuilder,
    private readonly router: Router,
    private readonly iStoreAuthService: IStoreAuthService,
    private readonly iStoreUserService: IStoreUserService,
    private readonly destroy$: IStoreDestroyService,
  ) { }

  public confirmationCodeLabel(): string {
    return `Confirmation code has been successfully sent to <br>${this.signForm.controls.phoneNumber.value}`;
  }

  private readonly confirmPhoneNumber = (confirmationCode: string | null) => this.iStoreAuthService
    .confirmPhoneNumber({
      confirmationCode,
      phoneNumber: PhoneNumber.cleanFormat(this.signForm.value.phoneNumber),
    })
    .pipe(
      map(user => {
        this.iStoreUserService.currentUser$.next(user);
        this.router.navigate([IStoreRoutingPathsEnum.Account, IStoreRoutingPathsEnum.Settings]);

        return false;
      }),
      catchError(error => {
        error?.error?.unableToConfirm && this.signForm.controls.confirmationCode.setErrors({ unableToConfirm: true });

        return of(true);
      }));

  public fillFields(user: IStoreUserInterface): void {
    this.signForm.controls.phoneNumber.patchValue(user.phoneNumber);
    this.signForm.controls.phoneNumber.markAsDirty();
  }

  public getCode(): void {
    const { phoneNumber } = this.signForm.controls;
    phoneNumber.setValidators([Validators.required, PhoneNumber.validator()]);
    phoneNumber.updateValueAndValidity();
    const { dirty, valid, value } = phoneNumber;

    if (dirty && valid && value) {
      this.iStoreAuthService
        .getConfirmationCode({ phoneNumber: PhoneNumber.cleanFormat(value) })
        .pipe(
          tap(() => {
            this.isPhoneForm$.next(false);
          }),
          tap(console.log),
          catchError(error => {
            const err = error?.error?.phoneNotUnique ? { phoneNotUnique: true } : { unknown: true };
            phoneNumber.setErrors(err);
            this.cdr.detectChanges();

            return of(null);
          }),
          takeUntil(this.destroy$))
        .subscribe();
    }
  }

  public getControlError(errors: ValidationErrors | null): string {
    const lastError = Object.keys(errors ?? { unknown: true }).pop();

    return signFormControlErrors[lastError ?? 'unknown'];
  }
}
