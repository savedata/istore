import { Component } from '@angular/core';

@Component({
  selector: 'istore-auth',
  template: '<router-outlet></router-outlet>',
})
export class IStoreAuthComponent { }
