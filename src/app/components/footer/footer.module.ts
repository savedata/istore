import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreFooterComponent } from './footer.component';

@NgModule({
  declarations: [IStoreFooterComponent],
  imports: [CommonModule],
  exports: [IStoreFooterComponent],
})
export class IStoreFooterModule { }
