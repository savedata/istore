import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';

import { IStoreNotFoundComponent } from './not-found.component';

const route: Route = {
  path: '',
  component: IStoreNotFoundComponent,
};

@NgModule({
  declarations: [IStoreNotFoundComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([route]),
  ],
  exports: [IStoreNotFoundComponent],
})
export class IStoreNotFoundModule { }
