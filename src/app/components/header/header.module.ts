import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreHeaderComponent } from './header.component';

@NgModule({
  declarations: [IStoreHeaderComponent],
  imports: [CommonModule],
  exports: [IStoreHeaderComponent],
})
export class IStoreHeaderModule { }
