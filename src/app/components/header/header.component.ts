import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreNavigationInterface } from '../../interfaces';
import { NAVIGATION_LIST } from '../../navigation-list.const';
import { IStoreAuthService, IStoreSidenavService, IStoreUserService } from '../../services';

const SIDENAV_LIST = {
  [IStoreRoutingPathsEnum.Admin]: true,
  [IStoreRoutingPathsEnum.Account]: true,
  [IStoreRoutingPathsEnum.Catalog]: false,
};

@Component({
  selector: 'istore-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class IStoreHeaderComponent {
  public readonly accountPath = NAVIGATION_LIST[IStoreRoutingPathsEnum.Account];
  public readonly adminPath = NAVIGATION_LIST[IStoreRoutingPathsEnum.Admin];
  public readonly pages: IStoreNavigationInterface[] = [
    NAVIGATION_LIST[IStoreRoutingPathsEnum.Catalog],
    NAVIGATION_LIST[IStoreRoutingPathsEnum.Contacts],
  ];

  public readonly currentUser$ = this.iStoreUserService.currentUser$;

  constructor(
    private readonly router: Router,
    private readonly iStoreAuthService: IStoreAuthService,
    private readonly iStoreSidenavService: IStoreSidenavService,
    private readonly iStoreUserService: IStoreUserService,
  ) {
    Object.entries(SIDENAV_LIST).forEach(([sidenav, defaultState]) => {
      this.iStoreSidenavService.switchSidenav(sidenav, defaultState);
    });
  }

  public get authPath(): IStoreNavigationInterface {
    return this.router.url.includes(IStoreRoutingPathsEnum.SignIn)
      ? NAVIGATION_LIST[IStoreRoutingPathsEnum.SignUp]
      : NAVIGATION_LIST[IStoreRoutingPathsEnum.SignIn];
  }

  public get isAdmin(): boolean {
    return this.iStoreUserService.isAdmin;
  }

  public logout(): void {
    this.iStoreAuthService.logout();
    this.iStoreUserService.currentUser$.next(null);
    this.router.navigate([IStoreRoutingPathsEnum.Catalog]);
  }

  public navigateTo(path: IStoreRoutingPathsEnum[]): void {
    this.router.navigate(path);
  }

  public switchSidenav(): void {
    const sidenav = Object
      .keys(SIDENAV_LIST)
      .reduce((currentSidenav, sidenav) => this.router.url.includes(sidenav) ? sidenav : currentSidenav, '');
    this.iStoreSidenavService.switchSidenav(sidenav);
  }
}
