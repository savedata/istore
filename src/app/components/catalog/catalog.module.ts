import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';

import { IStoreBoxModule, IStoreInputFieldModule, IStoreListModule, IStoreViewportModule } from '../ui';

import { IStoreCatalogComponent } from './catalog.component';

const route: Route = {
  path: '',
  component: IStoreCatalogComponent,
};

@NgModule({
  declarations: [IStoreCatalogComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([route]),
    IStoreBoxModule,
    IStoreInputFieldModule,
    IStoreListModule,
    IStoreViewportModule,
  ],
  exports: [IStoreCatalogComponent],
})
export class IStoreCatalogModule { }
