import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { tap } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreProductInterface } from '../../interfaces';
import { IStoreProductApiService, IStoreSidenavService } from '../../services';

@Component({
  selector: 'istore-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
})
export class IStoreCatalogComponent {
  @ViewChild('searchField', { static: true }) searchField: ElementRef<HTMLInputElement> | undefined;

  public readonly searchForm = this.formBuilder.group({
    search: [''],
  });

  public readonly showCart$ = this.iStoreSidenavService.sidenavStatus('cart');
  public readonly showSidenav$ = this.iStoreSidenavService
    .sidenavStatus(IStoreRoutingPathsEnum.Catalog)
    .pipe(
      tap(isOpen => {
        isOpen && this.searchField && this.searchField.nativeElement.focus();
      }));

  public readonly getProductList$ = this.iStoreProductApiService.getProductList();

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly iStoreProductApiService: IStoreProductApiService,
    private readonly iStoreSidenavService: IStoreSidenavService,
  ) { }

  public trackProducts(index: number, { id, slug }: IStoreProductInterface): number | string {
    return id ? id : slug;
  }
}
