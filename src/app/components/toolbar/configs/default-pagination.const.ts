import { IStorePaginationInterface } from '../../../interfaces';

export const DEFAULT_PAGINATION: IStorePaginationInterface = {
  count: 0,
  page: 1,
  perPage: 5,
  total: 0,
};
