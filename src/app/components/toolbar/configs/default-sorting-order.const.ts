import { IStoreSortingDirectionEnum, IStoreSortingOrderByEnum } from '../../../enums';
import { IStoreSortingOrderInterface } from '../../../interfaces';

export const DEFAULT_SORTING_ORDER: IStoreSortingOrderInterface = {
  direction: IStoreSortingDirectionEnum.Descending,
  orderBy: IStoreSortingOrderByEnum.UpdatedAt,
}
