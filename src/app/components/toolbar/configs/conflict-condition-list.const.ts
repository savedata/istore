import { IStoreFilterConditionsEnum } from '../../../enums';

export const CONDITION_CONFLICT_PRIORITY_LIST: { [key: string]: IStoreFilterConditionsEnum[]; } = {
  [IStoreFilterConditionsEnum.Is]: [
    IStoreFilterConditionsEnum.IsNot,
    IStoreFilterConditionsEnum.Contains,
    IStoreFilterConditionsEnum.DoesNotContain,
  ],
  [IStoreFilterConditionsEnum.IsNot]: [
    IStoreFilterConditionsEnum.Is,
  ],
  [IStoreFilterConditionsEnum.Contains]: [],
  [IStoreFilterConditionsEnum.DoesNotContain]: [],
  [IStoreFilterConditionsEnum.IsDate]: [],
  [IStoreFilterConditionsEnum.IsNotDate]: [],
  [IStoreFilterConditionsEnum.IsIn]: [],
  [IStoreFilterConditionsEnum.IsNotIn]: [],
};

export const CONDITION_CONFLICT_LIST: { [key: string]: IStoreFilterConditionsEnum[]; } = {
  [IStoreFilterConditionsEnum.Contains]: [
    IStoreFilterConditionsEnum.DoesNotContain,
    IStoreFilterConditionsEnum.Is,
    IStoreFilterConditionsEnum.IsNot,
  ],
  [IStoreFilterConditionsEnum.DoesNotContain]: [
    IStoreFilterConditionsEnum.Contains,
    IStoreFilterConditionsEnum.Is,
    IStoreFilterConditionsEnum.IsNot,
  ],
  [IStoreFilterConditionsEnum.StartsWith]: [
    IStoreFilterConditionsEnum.Is,
    IStoreFilterConditionsEnum.IsNot,
  ],
  [IStoreFilterConditionsEnum.EndsWith]: [
    IStoreFilterConditionsEnum.Is,
    IStoreFilterConditionsEnum.IsNot,
  ],
  [IStoreFilterConditionsEnum.Is]: [
    IStoreFilterConditionsEnum.IsNot,
    IStoreFilterConditionsEnum.Contains,
    IStoreFilterConditionsEnum.DoesNotContain,
  ],
  [IStoreFilterConditionsEnum.IsNot]: [
    IStoreFilterConditionsEnum.Is,
    IStoreFilterConditionsEnum.Contains,
    IStoreFilterConditionsEnum.DoesNotContain,
  ],
  [IStoreFilterConditionsEnum.IsDate]: [
    IStoreFilterConditionsEnum.IsNotDate,
  ],
  [IStoreFilterConditionsEnum.IsNotDate]: [
    IStoreFilterConditionsEnum.IsDate,
  ],
  [IStoreFilterConditionsEnum.IsIn]: [
    IStoreFilterConditionsEnum.IsNotIn,
  ],
  [IStoreFilterConditionsEnum.IsNotIn]: [
    IStoreFilterConditionsEnum.IsIn,
  ],
  [IStoreFilterConditionsEnum.AfterDate]: [
    IStoreFilterConditionsEnum.BeforeDate,
    IStoreFilterConditionsEnum.IsDate,
  ],
  [IStoreFilterConditionsEnum.BeforeDate]: [
    IStoreFilterConditionsEnum.AfterDate,
    IStoreFilterConditionsEnum.IsDate,
  ],
  [IStoreFilterConditionsEnum.BetweenDates]: [
    IStoreFilterConditionsEnum.AfterDate,
    IStoreFilterConditionsEnum.BeforeDate,
    IStoreFilterConditionsEnum.IsDate,
  ],
};
