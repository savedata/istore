import { IStoreFilterConditionsEnum, IStoreFilterTypesEnum } from '../../../enums';
import { IStoreSearchFiltersInterface } from '../../../interfaces';
import {
  IStoreToolbarConfigInterface,
  IStoreToolbarFilterInterface,
  IStoreToolbarSearchInterface,
} from '../interfaces';

import { DEFAULT_PAGINATION } from './default-pagination.const';
import { DEFAULT_SORTING_ORDER } from './default-sorting-order.const';

export const DEFAULT_TOOLBAR = 'default';

const FILTER_BY_ACTIVE_STATUS: IStoreToolbarFilterInterface = {
  fieldName: 'active',
  filterConditions: [{
    label: 'Active',
    value: IStoreFilterConditionsEnum.Is,
  },{
    label: 'Inactive',
    value: IStoreFilterConditionsEnum.IsNot,
  }],
  label: 'Active status',
  type: IStoreFilterTypesEnum.Boolean,
};

const FILTER_BY_SLUG: IStoreToolbarFilterInterface = {
  fieldName: 'slug',
  filterConditions: [{
    label: 'Contains',
    value: IStoreFilterConditionsEnum.Contains,
  },{
    label: 'Does not contains',
    value: IStoreFilterConditionsEnum.DoesNotContain,
  }],
  label: 'Slug',
  type: IStoreFilterTypesEnum.Options,
  options: [{
    label: 'Option 1',
    value: 'option-1',
  }],
};

const FILTER_BY_TITLE: IStoreToolbarFilterInterface = {
  fieldName: 'title',
  filterConditions: [{
    label: 'Contains',
    value: IStoreFilterConditionsEnum.Contains,
  },{
    label: 'Does not contains',
    value: IStoreFilterConditionsEnum.DoesNotContain,
  },{
    label: 'Is',
    value: IStoreFilterConditionsEnum.Is,
  },{
    label: 'Is not',
    value: IStoreFilterConditionsEnum.IsNot,
  }],
  label: 'Title',
  type: IStoreFilterTypesEnum.String,
};

export const TOOLBAR_CONFIG: IStoreToolbarConfigInterface = {
  addButton: false,
  options: {},
  pagination: DEFAULT_PAGINATION,
  searchFilters: [
    FILTER_BY_TITLE,
    FILTER_BY_SLUG,
    FILTER_BY_ACTIVE_STATUS,
  ],
  toolbarName: DEFAULT_TOOLBAR,
  sortingOrder: DEFAULT_SORTING_ORDER,
};

export const DEFAULT_SEARCH_CONFIG: IStoreToolbarSearchInterface = {
  pagination: DEFAULT_PAGINATION,
  searchFilters: { } as IStoreSearchFiltersInterface,
  sortingOrder: DEFAULT_SORTING_ORDER,
};
