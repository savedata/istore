import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { BehaviorSubject, map, switchMap, takeUntil, tap } from 'rxjs';

import { IStoreFilterConditionsEnum, IStoreFilterTypesEnum } from '../../enums';
import { IStoreSearchFilterInterface } from '../../interfaces';
import { IStoreDestroyService } from '../../services';

import { DEFAULT_TOOLBAR, TOOLBAR_CONFIG } from './configs';
import {
  IStoreToolbarConfigInterface,
  IStoreToolbarFilterConditionInterface,
  IStoreToolbarFilterInterface,
  IStoreToolbarFilterOptionsInterface,
} from './interfaces';
import { IStoreToolbarService } from './toolbar.service';

@Component({
  selector: 'istore-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [IStoreDestroyService],
})
export class IStoreToolbarComponent {
  public readonly counter$ = this.iStoreToolbarService.counter$;
  private readonly toolbarReady$ = new BehaviorSubject<string>(DEFAULT_TOOLBAR);
  private _config: IStoreToolbarConfigInterface = TOOLBAR_CONFIG;

  @Input() set config(value: IStoreToolbarConfigInterface) {
    if (value) {
      this._config = value;
      this.toolbarReady$.next(this.config.toolbarName);
    }
  }

  @Output() addAction = new EventEmitter<void>();

  public readonly toolbarForm = this.formBuilder.group({
    date: [''],
    dateSecond: [''],
    filterCondition: [0],
    option: [0],
    searchRequest: [''],
    searchFilter: [0],
  });

  public readonly filters$ = this.toolbarReady$
    .pipe(
      switchMap(toolbarName => this.iStoreToolbarService.getToolbarSearchConfig(toolbarName)),
      map(toolbar => this.iStoreToolbarService.flatFilterList(toolbar.searchFilters)));

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly iStoreToolbarService: IStoreToolbarService,
    private readonly destroy$: IStoreDestroyService,
  ) {
    const { date, dateSecond, filterCondition, option, searchRequest, searchFilter } = this.toolbarForm.controls;

    searchFilter
      .valueChanges
      .pipe(
        tap(value => {
          date.patchValue('');
          dateSecond.patchValue('');
          filterCondition.patchValue(0);
          option.patchValue(0);
          searchRequest.patchValue('');
        }),
        takeUntil(this.destroy$))
      .subscribe();
  }

  public get config(): IStoreToolbarConfigInterface {
    return this._config;
  }

  private get toolbarName(): string {
    return this.config.toolbarName;
  }

  private get searchFilter(): IStoreToolbarFilterInterface {
    return this.config.searchFilters[this.toolbarForm.value.searchFilter ?? 0];
  }

  public get searchFilterConditions(): IStoreToolbarFilterConditionInterface[] {
    return this.searchFilter.filterConditions;
  }

  public get searchFilterOptions(): IStoreToolbarFilterOptionsInterface[] {
    return this.searchFilter.options ?? [];
  }

  public get searchFilterType(): IStoreFilterTypesEnum {
    return this.searchFilter.type;
  }

  public get isBetweenDates(): boolean {
    return this.searchFilterConditions[this.toolbarForm.value.filterCondition ?? 0]
      .value === IStoreFilterConditionsEnum.BetweenDates;
  }

  public get isDateFilter(): boolean {
    return this.searchFilterType === IStoreFilterTypesEnum.Date;
  }

  public get isOptionsFilter(): boolean {
    return this.searchFilterType === IStoreFilterTypesEnum.Options;
  }

  public get isStringFilter(): boolean {
    return this.searchFilterType === IStoreFilterTypesEnum.String;
  }

  public applyFilter(): void {
    const { searchFilters } = this.config;
    const { date, dateSecond, filterCondition, option, searchFilter, searchRequest } = this.toolbarForm.value;
    const { fieldName, filterConditions, options, type } = searchFilters[searchFilter ?? 0];
    const getValue = (type: IStoreFilterTypesEnum): any => {
      switch (type) {
        case IStoreFilterTypesEnum.Date:
          return this.isBetweenDates && date && dateSecond
            ? [date, dateSecond]
            : !this.isBetweenDates && date
              ? date
              : null;
        case IStoreFilterTypesEnum.Boolean:
          return true;
        case IStoreFilterTypesEnum.Options:
          return options ? options[option ?? 0].value : null;
        default:
          return searchRequest ? searchRequest.toLowerCase() : null;
      }
    }

    const filter: IStoreSearchFilterInterface = {
      condition: filterConditions[filterCondition ?? 0].value,
      value: getValue(type),
    }

    filter.value && this.iStoreToolbarService.addSearchFilter(this.toolbarName, fieldName, filter);
  }

  public createByHand(): void {
    this.addAction.emit();
  }

  public getFilterDescribtion(propName: string, { condition, value }: IStoreSearchFilterInterface): string {
    const { label, filterConditions = [] } =  this.config.searchFilters
      .find(searchFilter => searchFilter.fieldName === propName) ?? { };
    const filter = filterConditions.find(({ value }) => value === condition) ?? { label: '' };

    return condition === IStoreFilterConditionsEnum.BetweenDates
      ? `${label} ${filter.label.split('dates')[0].toLowerCase()} ${value[0]} and ${value[1]}`
      : `${label} ${filter.label.split('date')[0].toLowerCase()} ${value}`;
  }

  public removeFilter(propName: string, filter: IStoreSearchFilterInterface): void {
    this.iStoreToolbarService.removeSearchFilter(this.toolbarName, propName, filter);
  }

  public resetFilters(): void {
    this.iStoreToolbarService.resetSearchFilters(this.toolbarName);
  }
}
