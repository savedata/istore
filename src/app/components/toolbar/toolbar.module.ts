import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreButtonModule, IStoreCheckboxModule, IStoreInputFieldModule, IStoreSelectModule } from '../ui';

import { IStoreToolbarComponent } from './toolbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [IStoreToolbarComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IStoreButtonModule,
    IStoreCheckboxModule,
    IStoreInputFieldModule,
    IStoreSelectModule,
  ],
  exports: [IStoreToolbarComponent],
})
export class IStoreToolbarModule { }
