import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { IStoreFilterConditionsEnum, IStoreSortingDirectionEnum, IStoreSortingOrderByEnum } from '../../enums';
import {
  IStorePropertyFilterInterface,
  IStoreSearchFilterInterface,
  IStoreSearchFiltersInterface,
} from '../../interfaces';

import {
  CONDITION_CONFLICT_LIST,
  CONDITION_CONFLICT_PRIORITY_LIST,
  DEFAULT_PAGINATION,
  DEFAULT_SEARCH_CONFIG,
  DEFAULT_SORTING_ORDER,
  DEFAULT_TOOLBAR,
} from './configs';
import { IStoreToolbarCounterInterface, IStoreToolbarSearchInterface } from './interfaces';

@Injectable({ providedIn: 'root' })
export class IStoreToolbarService {
  public readonly counter$ = new BehaviorSubject<IStoreToolbarCounterInterface>({ count: 0, total: 0 });
  private readonly toolbarList = new Map<string, BehaviorSubject<IStoreToolbarSearchInterface>>();

  public addSearchFilter(
    toolbarName = DEFAULT_TOOLBAR,
    propName: string,
    searchFilter: IStoreSearchFilterInterface,
  ): void {
    const searchConfig$ = this.toolbarList.get(toolbarName) ?? this.setToolbar(toolbarName);
    const searchConfig = searchConfig$.value;
    const filters = searchConfig.searchFilters[propName];

    const hasNoConflict = ({ condition, value }: IStoreSearchFilterInterface) => {
      return !((value === searchFilter.value
        && (CONDITION_CONFLICT_LIST[condition].includes(searchFilter.condition)
          || condition === searchFilter.condition))
          // || CONDITION_CONFLICT_PRIORITY_LIST[condition].includes(searchFilter.condition)
      );
    };

    if (filters) {
      searchConfig.searchFilters[propName] = filters.filter(hasNoConflict);
      searchConfig.searchFilters[propName].push(searchFilter);
    } else {
      searchConfig.searchFilters[propName] = [searchFilter];
    }

    searchConfig.pagination.page = 0;
    searchConfig$.next(searchConfig);
  }

  public applyFilters(entity: any, filters: IStorePropertyFilterInterface[]): boolean {
    return !filters.length ? true : filters
      .every(({ name, filter }) => {
        if (name in entity) {
          const propValue = typeof filter.value === 'string' ? entity[name].toLowerCase() : entity[name];

          switch (filter.condition) {
            case IStoreFilterConditionsEnum.Is:
              return propValue === filter.value;
            case IStoreFilterConditionsEnum.IsNot:
              return propValue !== filter.value;
            case IStoreFilterConditionsEnum.Contains:
              return propValue.includes(filter.value);
            case IStoreFilterConditionsEnum.DoesNotContain:
              return !propValue.includes(filter.value);
          }
        }

        return true;
      })
  }

  public destroyToolbar(toolbarName = DEFAULT_TOOLBAR): void {
    const searchConfig$ = this.toolbarList.get(toolbarName);
    if (searchConfig$) {
      searchConfig$.complete();
      this.toolbarList.delete(toolbarName);
    }
  }

  public flatFilterList(searchFilters: IStoreSearchFiltersInterface): IStorePropertyFilterInterface[] {
    return Object
      .entries(searchFilters)
      .flatMap(([name, filters]) => filters.flatMap(filter => ({ name, filter })));
  }

  public getToolbarSearchConfig(toolbarName = DEFAULT_TOOLBAR): BehaviorSubject<IStoreToolbarSearchInterface> {
    return this.toolbarList.get(toolbarName) ?? this.setToolbar(toolbarName);
  }

  public removeSearchFilter(
    toolbarName = DEFAULT_TOOLBAR,
    propName: string,
    searchFilter: IStoreSearchFilterInterface,
  ): void {
    const searchConfig$ = this.toolbarList.get(toolbarName);

    if (searchConfig$) {
      const searchConfig = searchConfig$.value;
      const filters = searchConfig.searchFilters[propName]
        .filter(({ condition, value }) => !(condition === searchFilter.condition && value === searchFilter.value));

      if (filters.length) {
        searchConfig.searchFilters[propName] = filters;
      } else {
        delete searchConfig.searchFilters[propName];
      }

      searchConfig.pagination.page = 0;
      searchConfig$.next(searchConfig);
    }
  }

  public resetSearchFilters(toolbarName = DEFAULT_TOOLBAR): void {
    const searchConfig$ = this.toolbarList.get(toolbarName);

    if (searchConfig$) {
      const searchConfig = searchConfig$.value;
      searchConfig.pagination.page = 0;
      searchConfig.searchFilters = { };
      searchConfig$.next(searchConfig);
    }
  }

  public setPagination(toolbarName = DEFAULT_TOOLBAR, pagination = DEFAULT_PAGINATION): void {
    const searchConfig$ = this.toolbarList.get(toolbarName) ?? this.setToolbar(toolbarName);
    const searchConfig = searchConfig$.value;
    searchConfig.pagination = pagination;
    searchConfig$.next(searchConfig);
  }

  public setSortingOrder(
    toolbarName = DEFAULT_TOOLBAR,
    field = IStoreSortingOrderByEnum.UpdatedAt,
    initialDirection = IStoreSortingDirectionEnum.Ascending,
  ): void {
    const searchConfig$ = this.toolbarList.get(toolbarName) ?? this.setToolbar(toolbarName);
    const searchConfig = searchConfig$.value;
    const { direction, orderBy } = searchConfig.sortingOrder;
    searchConfig.sortingOrder.direction = orderBy === field
      ? direction === IStoreSortingDirectionEnum.Descending
        ? IStoreSortingDirectionEnum.Ascending
        : IStoreSortingDirectionEnum.Descending
      : initialDirection;
    searchConfig.sortingOrder.orderBy = field;
    searchConfig.pagination.page = 0;
    searchConfig$.next(searchConfig);
  }

  public setToolbar(
    toolbarName = DEFAULT_TOOLBAR,
    searchConfig = DEFAULT_SEARCH_CONFIG,
  ): BehaviorSubject<IStoreToolbarSearchInterface> {
    const searchConfig$ = new BehaviorSubject<IStoreToolbarSearchInterface>(searchConfig);
    this.toolbarList.set(toolbarName, searchConfig$);

    return searchConfig$;
  }
}
