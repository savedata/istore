import { IStorePaginationInterface, IStoreSortingOrderInterface } from '../../../interfaces';

import { IStoreToolbarFilterInterface } from './toolbar-filter.interface';

export interface IStoreToolbarConfigInterface {
  addButton: boolean;
  options: { };
  pagination: IStorePaginationInterface;
  searchFilters: IStoreToolbarFilterInterface[];
  sortingOrder: IStoreSortingOrderInterface;
  toolbarName: string;
};
