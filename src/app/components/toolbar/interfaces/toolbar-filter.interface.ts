import { IStoreFilterConditionsEnum, IStoreFilterTypesEnum } from '../../../enums';

export interface IStoreToolbarFilterInterface {
  fieldName: string;
  filterConditions: IStoreToolbarFilterConditionInterface[];
  label: string;
  type: IStoreFilterTypesEnum;
  options?: IStoreToolbarFilterOptionsInterface[];
}

export interface IStoreToolbarFilterConditionInterface {
  label: string;
  value: IStoreFilterConditionsEnum;
}

export interface IStoreToolbarFilterOptionsInterface {
  label: string;
  value: string;
}
