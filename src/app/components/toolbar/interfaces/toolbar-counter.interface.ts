export interface IStoreToolbarCounterInterface {
  count: number;
  total: number;
}
