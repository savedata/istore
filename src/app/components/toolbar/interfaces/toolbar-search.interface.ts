import {
  IStorePaginationInterface,
  IStoreSearchFiltersInterface,
  IStoreSortingOrderInterface,
} from '../../../interfaces';

export interface IStoreToolbarSearchInterface {
  pagination: IStorePaginationInterface;
  searchFilters: IStoreSearchFiltersInterface;
  sortingOrder: IStoreSortingOrderInterface;
}
