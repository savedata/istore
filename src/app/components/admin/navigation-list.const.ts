import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreNavigationListInterface } from '../../interfaces';

export const ADMIN_NAVIGATION_LIST: IStoreNavigationListInterface = {
  [IStoreRoutingPathsEnum.Categories]: {
    path: [IStoreRoutingPathsEnum.Categories],
    title: 'Categories',
  },
  [IStoreRoutingPathsEnum.DeliveryMethods]: {
    path: [IStoreRoutingPathsEnum.DeliveryMethods],
    title: 'Delivery methods',
  },
  [IStoreRoutingPathsEnum.PaymentMethods]: {
    path: [IStoreRoutingPathsEnum.PaymentMethods],
    title: 'Payment methods',
  },
  [IStoreRoutingPathsEnum.Prices]: {
    path: [IStoreRoutingPathsEnum.Prices],
    title: 'Prices',
  },
  [IStoreRoutingPathsEnum.Products]: {
    path: [IStoreRoutingPathsEnum.Products],
    title: 'Products',
  },
  [IStoreRoutingPathsEnum.Sale]: {
    path: [IStoreRoutingPathsEnum.Sale],
    title: 'Sale',
  },
  [IStoreRoutingPathsEnum.Statuses]: {
    path: [IStoreRoutingPathsEnum.Statuses],
    title: 'Order statuses',
  },
  [IStoreRoutingPathsEnum.Users]: {
    path: [IStoreRoutingPathsEnum.Users],
    title: 'Users',
  },
};
