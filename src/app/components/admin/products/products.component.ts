import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

import { IStoreRoutingPathsEnum } from '../../../enums';
import { IStoreDestroyService } from '../../../services';

@Component({
  selector: 'istore-products',
  templateUrl: './products.component.html',
  providers: [IStoreDestroyService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IStoreProductsComponent {
  constructor(private readonly router: Router) { }

  public get productId(): string {
    return this.router.url.split(IStoreRoutingPathsEnum.Editor)[1];
  }
}
