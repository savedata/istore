import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, map, of, switchMap, takeUntil, tap } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../../../enums';
import { IStoreProductInterface } from '../../../interfaces';
import { IStoreDestroyService, IStoreProductApiService, IStoreProductService } from '../../../services';
import { FormEntity } from '@istore/utils';

const controlErrors: { [key: string]: string } = {
  skuNotUnique: 'SKU is not unique',
  slugNotUnique: 'Slug is not unique',
  titleNotUnique: 'Title is not unique',
  required: '{fieldName} is required',
  unknown: 'Unknown error',
};

@Component({
  selector: 'istore-product-editor',
  templateUrl: './product-editor.component.html',
  providers: [IStoreDestroyService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IStoreProductEditorComponent {
  public readonly productForm = this.formBuilder.group({
    id: [NaN],
    active: [true],
    brand: [''],
    description: [''],
    quantity: [0],
    sku: [''],
    slug: [''],
    title: [''],
  });

  private productIndex = -1;
  private readonly productId = Number(this.activatedRoute.snapshot.params['entityId']);
  private readonly productList$ = this.iStoreProductService.productList$;
  public readonly initForm$ = this.productList$
    .pipe(
      switchMap(productList => {
        if (this.productId) {
          const productIndex = productList.findIndex(product => product.id === this.productId);
          this.productIndex = productIndex;

          return productIndex >=0
            ? of(productList[productIndex])
            : this.iStoreProductApiService.getProduct(this.productId);
        } else {
          return of(undefined);
        }
      }),
      map(product => {
        product && this.productForm.patchValue(product);

        return true;
      }),
      catchError(error => {
        error?.error?.notFound && this.productForm.setErrors({ notFound: true });
        this.cdr.detectChanges();

        return of(true);
      }));

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly iStoreProductApiService: IStoreProductApiService,
    private readonly iStoreProductService: IStoreProductService,
    private readonly destroy$: IStoreDestroyService,
  ) { }

  public saveProduct(): void {
    const { id, sku, slug, title } = this.productForm.controls;
    sku.setValidators([Validators.required]);
    sku.updateValueAndValidity();
    slug.setValidators([Validators.required]);
    slug.updateValueAndValidity();
    title.setValidators([Validators.required]);
    title.updateValueAndValidity();
    const { dirty, valid } = this.productForm;

    if (dirty && valid) {
      const product = FormEntity.getFromDirtyFields<IStoreProductInterface>(this.productForm);
      const saveAction$ = id.value
        ? this.iStoreProductApiService.updateProduct(product)
        : this.iStoreProductApiService.createProduct(product);

      saveAction$
        .pipe(
          tap(product => {
            if (this.productIndex >= 0) {
              this.productList$.value.splice(this.productIndex, 1, product);
            } else {
              this.productList$.value.push(product);
            }

            this.productList$.next(this.productList$.value);
            const turnBack = id.value ? '../../' : '../';
            this.router.navigate([turnBack, IStoreRoutingPathsEnum.EntityList], { relativeTo: this.activatedRoute });
          }),
          catchError(error => {
            error?.error?.skuNotUnique && sku.setErrors({ skuNotUnique: true });
            error?.error?.slugNotUnique && slug.setErrors({ slugNotUnique: true });
            error?.error?.titleNotUnique && title.setErrors({ titleNotUnique: true });
            this.cdr.detectChanges();

            return of(null);
          }),
          takeUntil(this.destroy$))
        .subscribe();
    }
  }

  public getControlError(errors: ValidationErrors | null, fieldName = ''): string {
    const lastError = Object.keys(errors ?? { unknown: true }).pop();

    return controlErrors[lastError ?? 'unknown'].replace('{fieldName}', fieldName);
  }
}
