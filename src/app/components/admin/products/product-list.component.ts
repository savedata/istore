import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, tap } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../../../enums';
import { IStoreProductInterface } from '../../../interfaces';
import { IStoreDestroyService, IStoreProductApiService, IStoreProductService } from '../../../services';

@Component({
  selector: 'istore-product-list',
  templateUrl: './product-list.component.html',
  providers: [IStoreDestroyService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IStoreProductListComponent implements OnInit {
  public readonly productList$ = this.iStoreProductService.productList$;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly iStoreProductApiService: IStoreProductApiService,
    private readonly iStoreProductService: IStoreProductService,
    private readonly destroy$: IStoreDestroyService,
  ) { }

  ngOnInit(): void {
    !this.productList$.value.length && this.iStoreProductApiService
      .getProductList()
      .pipe(
        tap(productList => {
          this.productList$.next(productList);
        }),
        takeUntil(this.destroy$))
      .subscribe();
  }

  public openEditor(productId: number, index: number): void {
    this.router.navigate(['../', IStoreRoutingPathsEnum.Editor, productId], { relativeTo: this.activatedRoute });
  }

  public removeProduct(productId: number, index: number): void {
    this.iStoreProductApiService
      .deleteProduct(productId)
        .pipe(
          tap(product => {
            this.productList$.value.splice(index, 1);
            this.productList$.next(this.productList$.value);
          }),
          takeUntil(this.destroy$))
        .subscribe();
  }

  public trackProducts(index: number, { id, slug }: IStoreProductInterface): number | string {
    return id ? id : slug;
  }
}
