import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, of, OperatorFunction } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreDestroyService, IStoreSidenavService, IStoreUserApiService, IStoreUserService } from '../../services';

import { ADMIN_NAVIGATION_LIST } from './navigation-list.const';

@Component({
  selector: 'istore-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [IStoreDestroyService],
})
export class IStoreAdminComponent {
  public readonly adminNavList = [
    ADMIN_NAVIGATION_LIST[IStoreRoutingPathsEnum.Categories],
    ADMIN_NAVIGATION_LIST[IStoreRoutingPathsEnum.DeliveryMethods],
    ADMIN_NAVIGATION_LIST[IStoreRoutingPathsEnum.PaymentMethods],
    ADMIN_NAVIGATION_LIST[IStoreRoutingPathsEnum.Prices],
    ADMIN_NAVIGATION_LIST[IStoreRoutingPathsEnum.Products],
    ADMIN_NAVIGATION_LIST[IStoreRoutingPathsEnum.Sale],
    ADMIN_NAVIGATION_LIST[IStoreRoutingPathsEnum.Statuses],
    ADMIN_NAVIGATION_LIST[IStoreRoutingPathsEnum.Users],
  ];

  public readonly showSidenav$ = this.iStoreSidenavService.sidenavStatus(IStoreRoutingPathsEnum.Admin);

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly iStoreUserService: IStoreUserService,
    private readonly iStoreSidenavService: IStoreSidenavService,
    private readonly iStoreUserApiService: IStoreUserApiService,
    private readonly destroy$: IStoreDestroyService,
  ) { }

  public closeAdmin(): void {
    this.router.navigateByUrl(this.activatedRoute.snapshot.data['previousPath']);
  }

  private errorHandler(): OperatorFunction<any, any> {
    return catchError((error) => {

      return of(error);
    });
  }
}
