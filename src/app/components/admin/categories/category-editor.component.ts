import { HttpEvent } from '@angular/common/http';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { FormBuilder, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, map, of, switchMap, take, takeUntil, tap } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../../../enums';
import { IStoreCategoryInterface, IStoreMediaInterface } from '../../../interfaces';
import {
  IStoreDestroyService,
  IStoreMediaService,
  IStoreProductApiService,
  IStoreProductService,
} from '../../../services';
import { FormEntity } from '@istore/utils';

const controlErrors: { [key: string]: string } = {
  slugNotUnique: 'Slug is not unique',
  titleNotUnique: 'Title is not unique',
  required: '{fieldName} is required',
  unknown: 'Unknown error',
};

@Component({
  selector: 'istore-category-editor',
  templateUrl: './category-editor.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [IStoreDestroyService],
})
export class IStoreCategoryEditorComponent {
  public readonly categoryForm = this.formBuilder.group({
    id: [NaN],
    active: [true],
    description: [''],
    defaultImage: [''],
    media: [[] as number[] | Partial<IStoreMediaInterface>[]],
    slug: [''],
    title: [''],
  });

  private categoryIndex = -1;
  private readonly categoryId = Number(this.activatedRoute.snapshot.params['entityId']);
  private readonly categoryList$ = this.iStoreProductService.categoryList$;
  public readonly initForm$ = this.categoryList$
    .pipe(
      take(1),
      switchMap(categoryList => {
        if (this.categoryId) {
          const categoryIndex = categoryList.findIndex(category => category.id === this.categoryId);
          this.categoryIndex = categoryIndex;

          return categoryIndex >=0
            ? of(categoryList[categoryIndex])
            : this.iStoreProductApiService.getCategory(this.categoryId);
        } else {
          return of(undefined);
        }
      }),
      map(category => {
        category && this.categoryForm.patchValue(category);

        return true;
      }),
      catchError(error => {
        error?.error?.notFound && this.categoryForm.setErrors({ notFound: true });
        this.cdr.detectChanges();

        return of(true);
      }));

  private readonly slugValue$ = this.categoryForm.controls.title
    .valueChanges
    .pipe(
      tap(title => {
        if (title) {
          const regExp = /[^0-9a-zA-Z]/g;
          const slugLink = title.toLowerCase().replace(regExp, ' ').trim().replace(/\s+/g, '-');
          this.categoryForm.controls.slug.patchValue(slugLink);
        }
      }));

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly iStoreMediaService: IStoreMediaService,
    private readonly iStoreProductApiService: IStoreProductApiService,
    private readonly iStoreProductService: IStoreProductService,
    private readonly destroy$: IStoreDestroyService,
  ) {
    this.slugValue$.pipe(takeUntil(this.destroy$)).subscribe();
  }

  get mediaList(): IStoreMediaInterface[] {
    const { media } = this.categoryForm.value;

    return media ? (media as IStoreMediaInterface[]).sort((prev, curr) => prev.id - curr.id) : [];
  }

  public setDefault(file: number | IStoreMediaInterface): void {
    const { controls } = this.categoryForm;

    controls.id.value && typeof file !== 'number' && !file.default && this.iStoreMediaService
      .setDefaultImage(file)
      .pipe(
        tap(media => {
          const defaultImage = media.find(file => file.default)?.path;
          controls.media.patchValue(media);
          defaultImage && controls.defaultImage.patchValue(defaultImage);

          if (defaultImage && this.categoryIndex >= 0) {
            const category = this.categoryList$.value[this.categoryIndex];
            category.defaultImage = defaultImage;
            category.media = media;
            this.categoryList$.value.splice(this.categoryIndex, 1, category);
            this.categoryList$.next(this.categoryList$.value);
          }

          this.cdr.detectChanges();
        }),
        takeUntil(this.destroy$))
      .subscribe();
  }

  public removeImage(file: number | IStoreMediaInterface, index: number): void {
    typeof file !== 'number' && !file.default && this.iStoreMediaService
      .removeFile(file.id)
      .pipe(
        tap(() => {
          const { media } = this.categoryForm.controls;
          const { value } = media;
          value?.splice(index, 1);
          media.patchValue(value);
          media.markAsDirty();

          this.cdr.detectChanges();
        }),
        takeUntil(this.destroy$))
      .subscribe();
  }

  public onFileChange(evt: Event): void {
    const { files } = evt.target as HTMLInputElement;
    files && this.iStoreMediaService
      .uploadFiles(Array.from<File>(files))
      .pipe(
        tap((event: HttpEvent<any>) => {
          if (event.type === 4) {
            const { defaultImage, media } = this.categoryForm.controls;
            defaultImage.patchValue(event.body[0].path);
            const { value } = media;
            value?.push(...event.body);
            media.patchValue(value);
            media.markAsDirty();

            this.cdr.detectChanges();
          }
        }))
      .subscribe();
  }

  public saveCategory(): void {
    const { id, slug, title } = this.categoryForm.controls;
    slug.setValidators([Validators.required]);
    slug.updateValueAndValidity();
    title.setValidators([Validators.required]);
    title.updateValueAndValidity();
    const { dirty, valid } = this.categoryForm;

    if (dirty && valid) {
      const category = FormEntity.getFromDirtyFields<IStoreCategoryInterface>(this.categoryForm);
      const saveAction$ = id.value
        ? this.iStoreProductApiService.updateCategory(category)
        : this.iStoreProductApiService.createCategory(category);

      saveAction$
        .pipe(
          tap(category => {
            if (this.categoryIndex >= 0) {
              this.categoryList$.value.splice(this.categoryIndex, 1, category);
            } else {
              this.categoryList$.value.push(category);
            }

            this.categoryList$.next(this.categoryList$.value);
            const turnBack = id.value ? '../../' : '../';
            this.router.navigate([turnBack, IStoreRoutingPathsEnum.EntityList], { relativeTo: this.activatedRoute });
          }),
          catchError(error => {
            error?.error?.slugNotUnique && slug.setErrors({ slugNotUnique: true });
            error?.error?.titleNotUnique && title.setErrors({ titleNotUnique: true });
            this.cdr.detectChanges();

            return of(null);
          }),
          takeUntil(this.destroy$))
        .subscribe();
    }
  }

  public getControlError(errors: ValidationErrors | null, fieldName = ''): string {
    const lastError = Object.keys(errors ?? { unknown: true }).pop();

    return controlErrors[lastError ?? 'unknown'].replace('{fieldName}', fieldName);
  }
}
