import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

import { IStoreRoutingPathsEnum } from '../../../enums';
import { IStoreDestroyService } from '../../../services';

@Component({
  selector: 'istore-categories',
  templateUrl: './categories.component.html',
  providers: [IStoreDestroyService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IStoreCategoriesComponent {
  constructor(private readonly router: Router) { }

  public get categoryId(): string {
    return this.router.url.split(IStoreRoutingPathsEnum.Editor)[1];
  }
}
