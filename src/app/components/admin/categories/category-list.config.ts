import {
  IStoreFilterConditionsEnum,
  IStoreFilterTypesEnum,
  IStoreSortingDirectionArrowEnum,
  IStoreSortingDirectionEnum,
  IStoreSortingOrderByEnum,
} from '../../../enums';
import { IStoreListFieldInterface, IStorePaginationInterface, IStoreSortingOrderInterface } from '../../../interfaces';
import { IStoreToolbarConfigInterface, IStoreToolbarFilterInterface } from '../../toolbar';

const FILTER_BY_ACTIVE_STATUS: IStoreToolbarFilterInterface = {
  fieldName: 'active',
  filterConditions: [{
    label: 'Active',
    value: IStoreFilterConditionsEnum.Is,
  },{
    label: 'Inactive',
    value: IStoreFilterConditionsEnum.IsNot,
  }],
  label: 'Active status',
  type: IStoreFilterTypesEnum.Boolean,
};

const FILTER_BY_SLUG: IStoreToolbarFilterInterface = {
  fieldName: 'slug',
  filterConditions: [{
    label: 'Contains',
    value: IStoreFilterConditionsEnum.Contains,
  },{
    label: 'Does not contain',
    value: IStoreFilterConditionsEnum.DoesNotContain,
  }],
  label: 'Slug',
  type: IStoreFilterTypesEnum.Options,
  options: [{
    label: 'Option 1',
    value: 'option-1',
  }],
};

const FILTER_BY_TITLE: IStoreToolbarFilterInterface = {
  fieldName: 'title',
  filterConditions: [{
    label: 'Contains',
    value: IStoreFilterConditionsEnum.Contains,
  },{
    label: 'Does not contain',
    value: IStoreFilterConditionsEnum.DoesNotContain,
  },{
    label: 'Ends with',
    value: IStoreFilterConditionsEnum.EndsWith,
  },{
    label: 'Starts with',
    value: IStoreFilterConditionsEnum.StartsWith,
  },{
    label: 'Is',
    value: IStoreFilterConditionsEnum.Is,
  },{
    label: 'Is not',
    value: IStoreFilterConditionsEnum.IsNot,
  }],
  label: 'Title',
  type: IStoreFilterTypesEnum.String,
};

const FILTER_BY_CREATION_DATE: IStoreToolbarFilterInterface = {
  fieldName: 'createdAt',
  filterConditions: [{
    label: 'After date',
    value: IStoreFilterConditionsEnum.AfterDate,
  },{
    label: 'Before date',
    value: IStoreFilterConditionsEnum.BeforeDate,
  },{
    label: 'Between dates',
    value: IStoreFilterConditionsEnum.BetweenDates,
  },{
    label: 'For date',
    value: IStoreFilterConditionsEnum.IsDate,
  },{
    label: 'Excluding date',
    value: IStoreFilterConditionsEnum.IsNotDate,
  }],
  label: 'Creation date',
  type: IStoreFilterTypesEnum.Date,
};

const FILTER_BY_LATEST_CHANGES: IStoreToolbarFilterInterface = {
  fieldName: 'updatedAt',
  filterConditions: [{
    label: 'After date',
    value: IStoreFilterConditionsEnum.AfterDate,
  },{
    label: 'Before date',
    value: IStoreFilterConditionsEnum.BeforeDate,
  },{
    label: 'Between dates',
    value: IStoreFilterConditionsEnum.BetweenDates,
  },{
    label: 'For date',
    value: IStoreFilterConditionsEnum.IsDate,
  },{
    label: 'Excluding date',
    value: IStoreFilterConditionsEnum.IsNotDate,
  }],
  label: 'Latest changes',
  type: IStoreFilterTypesEnum.Date,
};

const FILTER_BY_MEDIA: IStoreToolbarFilterInterface = {
  fieldName: 'id',
  filterConditions: [{
    label: 'Not empty',
    value: IStoreFilterConditionsEnum.IsNotEmpty,
  },{
    label: 'Empty',
    value: IStoreFilterConditionsEnum.IsEmpty,
  }],
  label: 'Image list',
  type: IStoreFilterTypesEnum.Boolean,
};

const SEARCH_FILTERS: IStoreToolbarFilterInterface[] = [
  // FILTER_BY_MEDIA,
  FILTER_BY_TITLE,
  FILTER_BY_SLUG,
  FILTER_BY_ACTIVE_STATUS,
  FILTER_BY_CREATION_DATE,
  FILTER_BY_LATEST_CHANGES,
];

const PAGINATION_CONFIG: IStorePaginationInterface = {
  count: 0,
  page: 1,
  perPage: 2,
  total: 0,
};

const SORTING_ORDER: IStoreSortingOrderInterface = {
  direction: IStoreSortingDirectionEnum.Descending,
  orderBy: IStoreSortingOrderByEnum.UpdatedAt,
};

export const CATEGORY_TOOLBAR = 'category';

export const TOOLBAR_CONFIG: IStoreToolbarConfigInterface = {
  addButton: true,
  options: {},
  pagination: PAGINATION_CONFIG,
  searchFilters: SEARCH_FILTERS,
  sortingOrder: SORTING_ORDER,
  toolbarName: CATEGORY_TOOLBAR,
};

export const LIST_FIELDS: IStoreListFieldInterface[] = [{
  id: IStoreSortingOrderByEnum.Tite,
  label: 'Title',
  arrow: {
    asc: IStoreSortingDirectionArrowEnum.Up,
    desc: IStoreSortingDirectionArrowEnum.Down,
  },
  defaultSortDirection: IStoreSortingDirectionEnum.Ascending,
},{
  id: IStoreSortingOrderByEnum.Description,
  label: 'Description',
  arrow: {
    asc: IStoreSortingDirectionArrowEnum.Up,
    desc: IStoreSortingDirectionArrowEnum.Down,
  },
  defaultSortDirection: IStoreSortingDirectionEnum.Ascending,
},{
  id: IStoreSortingOrderByEnum.Slug,
  label: 'Slug',
  arrow: {
    asc: IStoreSortingDirectionArrowEnum.Up,
    desc: IStoreSortingDirectionArrowEnum.Down,
  },
  defaultSortDirection: IStoreSortingDirectionEnum.Ascending,
},{
  id: IStoreSortingOrderByEnum.ActiveStatus,
  label: 'Active status',
  arrow: {
    asc: IStoreSortingDirectionArrowEnum.Down,
    desc: IStoreSortingDirectionArrowEnum.Up,
  },
  defaultSortDirection: IStoreSortingDirectionEnum.Descending,
},{
  id: IStoreSortingOrderByEnum.UpdatedAt,
  label: 'Last changes',
  arrow: {
    asc: IStoreSortingDirectionArrowEnum.Down,
    desc: IStoreSortingDirectionArrowEnum.Up,
  },
  defaultSortDirection: IStoreSortingDirectionEnum.Descending,
}];
