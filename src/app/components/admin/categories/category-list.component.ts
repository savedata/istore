import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, map, merge, skip, switchMap, take, takeUntil, tap, withLatestFrom } from 'rxjs';

import { IStoreRoutingPathsEnum, IStoreSortingDirectionEnum, IStoreSortingOrderByEnum } from '../../../enums';
import { IStoreCategoryInterface, IStoreListFieldInterface, IStoreSearchDataInterface } from '../../../interfaces';
import { IStoreDestroyService, IStoreProductApiService, IStoreProductService } from '../../../services';
import { IStoreToolbarService } from '../../toolbar';

import { CATEGORY_TOOLBAR, LIST_FIELDS, TOOLBAR_CONFIG } from './category-list.config';

@Component({
  selector: 'istore-category-list',
  templateUrl: './category-list.component.html',
  providers: [IStoreDestroyService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IStoreCategoryListComponent {
  public searchData: IStoreSearchDataInterface = {
    orderBy: IStoreSortingOrderByEnum.UpdatedAt,
    direction: IStoreSortingDirectionEnum.Descending,
    page: 0,
    perPage: 20,
    total: 0,
  };

  public readonly listFields = LIST_FIELDS;
  public readonly toolbarConfig = TOOLBAR_CONFIG;

  public readonly loading$ = new BehaviorSubject<boolean>(false);
  private readonly getLocalCategoryList$ = this.iStoreProductService.categoryList$.pipe(take(1));
  private readonly getGlobalCategoryList$ = this.iStoreToolbarService
    .getToolbarSearchConfig(CATEGORY_TOOLBAR)
    .pipe(
      skip(1),
      switchMap(({ pagination, searchFilters, sortingOrder }) => {
        if (!pagination.page) {
          this.iStoreProductService.categoryList$.next([]);
          pagination.page = 1;
        }

        this.searchData = {
          configuration: searchFilters,
          ...pagination,
          ...sortingOrder,
        }
        this.loading$.next(true);

        return this.iStoreProductApiService.getCategoryList(this.searchData);
      }),
      withLatestFrom(this.iStoreProductService.categoryList$),
      map(([{ collection, pagination }, categoryList]): IStoreCategoryInterface[] => {
        this.searchData.total = pagination.total;
        categoryList = categoryList
          .filter(existedCategory => !collection.some(category => category.id === existedCategory.id));
        categoryList.push(...collection);
        this.iStoreProductService.categoryList$.next(categoryList);
        this.iStoreToolbarService.counter$.next(pagination);
        this.loading$.next(false);

        return categoryList;
      }));

  public readonly categoryList$ = merge(
    this.getLocalCategoryList$,
    this.getGlobalCategoryList$,
  );

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly iStoreProductApiService: IStoreProductApiService,
    private readonly iStoreProductService: IStoreProductService,
    private readonly iStoreToolbarService: IStoreToolbarService,
    private readonly destroy$: IStoreDestroyService,
  ) { }

  public loadMore(perPage: number): void {
    const count = this.iStoreProductService.categoryList$.value.length;
    const { total } = this.searchData;
    const nextPage = Math.floor(count / perPage) + 1;

    this.iStoreToolbarService
      .setPagination(CATEGORY_TOOLBAR, { count, page: nextPage, perPage, total });
  }

  public openEditor(categoryId?: number): void {
    const path = ['../', IStoreRoutingPathsEnum.Editor];
    categoryId && path.push(`${categoryId}`);
    this.router.navigate(path, { relativeTo: this.activatedRoute });
  }

  public orderBy({ id, defaultSortDirection }: IStoreListFieldInterface): void {
    this.iStoreToolbarService.setSortingOrder(CATEGORY_TOOLBAR, id, defaultSortDirection);
  }

  public removeCategory(categoryId: number, index: number): void {
    this.iStoreProductApiService
      .deleteCategory(categoryId)
        .pipe(
          tap(category => {
            this.iStoreProductService.categoryList$.value.splice(index, 1);
            this.iStoreProductService.categoryList$.next(this.iStoreProductService.categoryList$.value);
          }),
          takeUntil(this.destroy$))
        .subscribe();
  }

  public trackCategories(index: number, { id, slug }: IStoreCategoryInterface): number | string {
    return id ? id : slug;
  }
}
