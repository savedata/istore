import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';

import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreDailyStatusPipe, IStoreMediaUrlPipe } from '../../pipes';
import { IStoreToolbarModule } from '../toolbar';
import {
  IStoreBoxModule,
  IStoreButtonModule,
  IStoreCheckboxModule,
  IStoreInputFieldModule,
  IStoreListModule,
  IStoreSelectModule,
  IStoreSwitcherModule,
  IStoreTabsModule,
  IStoreViewportModule,
} from '../ui';

import { IStoreAdminComponent } from './admin.component';
import { IStoreCategoriesComponent, IStoreCategoryEditorComponent, IStoreCategoryListComponent } from './categories';
import { IStoreProductEditorComponent, IStoreProductListComponent, IStoreProductsComponent } from './products';

const generalPath = 'iStore | Admin panel | ';

const route: Route = {
  path: '',
  component: IStoreAdminComponent,
  children: [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: IStoreRoutingPathsEnum.Categories,
    },
    {
      path: IStoreRoutingPathsEnum.Categories,
      component: IStoreCategoriesComponent,
      title: `${generalPath}Categories`,
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: IStoreRoutingPathsEnum.EntityList,
        },
        {
          path: IStoreRoutingPathsEnum.EntityList,
          component: IStoreCategoryListComponent,
        },
        {
          path: IStoreRoutingPathsEnum.EntityEditor,
          component: IStoreCategoryEditorComponent,
        },
        {
          path: IStoreRoutingPathsEnum.Editor,
          component: IStoreCategoryEditorComponent,
        },
      ],
    },
    {
      path: IStoreRoutingPathsEnum.DeliveryMethods,
      component: IStoreCategoriesComponent,
      title: `${generalPath}Delivery methods`,
    },
    {
      path: IStoreRoutingPathsEnum.PaymentMethods,
      component: IStoreCategoriesComponent,
      title: `${generalPath}Payment methods`,
    },
    {
      path: IStoreRoutingPathsEnum.Prices,
      component: IStoreCategoriesComponent,
      title: `${generalPath}Prices`,
    },
    {
      path: IStoreRoutingPathsEnum.Products,
      component: IStoreProductsComponent,
      title: `${generalPath}Products`,
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: IStoreRoutingPathsEnum.EntityList,
        },
        {
          path: IStoreRoutingPathsEnum.EntityList,
          component: IStoreProductListComponent,
        },
        {
          path: IStoreRoutingPathsEnum.EntityEditor,
          component: IStoreProductEditorComponent,
        },
        {
          path: IStoreRoutingPathsEnum.Editor,
          component: IStoreProductEditorComponent,
        },
      ],
    },
    {
      path: IStoreRoutingPathsEnum.Sale,
      component: IStoreCategoriesComponent,
      title: `${generalPath}Sale`,
    },
    {
      path: IStoreRoutingPathsEnum.Statuses,
      component: IStoreCategoriesComponent,
      title: `${generalPath}Order statuses`,
    },
    {
      path: IStoreRoutingPathsEnum.Users,
      component: IStoreCategoriesComponent,
      title: `${generalPath}Users`,
    },
  ],
};

@NgModule({
  declarations: [
    IStoreAdminComponent,
    IStoreCategoriesComponent,
    IStoreCategoryEditorComponent,
    IStoreCategoryListComponent,
    IStoreProductEditorComponent,
    IStoreProductListComponent,
    IStoreProductsComponent,

    IStoreDailyStatusPipe,
    IStoreMediaUrlPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([route]),
    IStoreButtonModule,
    IStoreBoxModule,
    IStoreCheckboxModule,
    IStoreInputFieldModule,
    IStoreListModule,
    IStoreSelectModule,
    IStoreSwitcherModule,
    IStoreTabsModule,
    IStoreToolbarModule,
    IStoreViewportModule,
  ],
})
export class IStoreAdminModule { }
