export * from './account';
export * from './auth';
export * from './catalog';
export * from './contacts';
export * from './footer';
export * from './header';
export * from './not-found';
// export * from '.';
