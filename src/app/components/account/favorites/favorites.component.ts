import { Component } from '@angular/core';

@Component({
  selector: 'istore-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
})
export class IStoreFavoritesComponent { }
