import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreNavigationListInterface } from '../../interfaces';

export const ACCOUNT_NAVIGATION_LIST: IStoreNavigationListInterface = {
  [IStoreRoutingPathsEnum.Orders]: {
    path: [IStoreRoutingPathsEnum.Orders],
    title: 'Orders',
  },
  [IStoreRoutingPathsEnum.Favorites]: {
    path: [IStoreRoutingPathsEnum.Favorites],
    title: 'Favorites',
  },
  [IStoreRoutingPathsEnum.Delivery]: {
    path: [IStoreRoutingPathsEnum.Delivery],
    title: 'Delivery',
  },
  [IStoreRoutingPathsEnum.Settings]: {
    path: [IStoreRoutingPathsEnum.Settings],
    title: 'Settings',
  },
};
