import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, of, OperatorFunction, takeUntil, tap } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreDestroyService, IStoreSidenavService, IStoreUserApiService, IStoreUserService } from '../../services';

import { ACCOUNT_NAVIGATION_LIST } from './navigation-list.const';

@Component({
  selector: 'istore-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  providers: [IStoreDestroyService],
})
export class IStoreAccountComponent {
  public readonly accountNavList = [
    ACCOUNT_NAVIGATION_LIST[IStoreRoutingPathsEnum.Orders],
    ACCOUNT_NAVIGATION_LIST[IStoreRoutingPathsEnum.Favorites],
    ACCOUNT_NAVIGATION_LIST[IStoreRoutingPathsEnum.Delivery],
    ACCOUNT_NAVIGATION_LIST[IStoreRoutingPathsEnum.Settings],
  ];

  public readonly allowedContent = [{
    title: 'Public content',
    path: 'all',
  }];

  public readonly showSidenav$ = this.iStoreSidenavService.sidenavStatus(IStoreRoutingPathsEnum.Account);

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly iStoreUserService: IStoreUserService,
    private readonly iStoreSidenavService: IStoreSidenavService,
    private readonly iStoreUserApiService: IStoreUserApiService,
    private readonly destroy$: IStoreDestroyService,
  ) {
    this.isAllowForAdmin && this.allowedContent.push({
      title: 'Admin content',
      path: 'admin',
    });

    this.isAllowForModerator && this.allowedContent.push({
      title: 'Moderator content',
      path: 'mod',
    });

    this.isAllowForUser && this.allowedContent.push({
      title: 'User content',
      path: 'user',
    });
  }

  private get isAllowForAdmin(): boolean {
    return this.iStoreUserService.isAdmin;
  }

  private get isAllowForModerator(): boolean {
    return this.iStoreUserService.isModerator;
  }

  private get isAllowForUser(): boolean {
    return this.iStoreUserService.isUser;
  }

  public closeAccount(): void {
    this.router.navigateByUrl(this.activatedRoute.snapshot.data['previousPath']);
  }

  public getContent(type: string): void {
    this.iStoreUserApiService
      .getContent(type)
      .pipe(
        tap(({ message }) => {
        }),
        this.errorHandler(),
        takeUntil(this.destroy$))
      .subscribe();
  }

  private errorHandler(): OperatorFunction<any, any> {
    return catchError((error) => {

      return of(error);
    });
  }
}
