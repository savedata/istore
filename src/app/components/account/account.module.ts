import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, RouterModule } from '@angular/router';

import { IStoreRoutingPathsEnum } from '../../enums';
import { IStoreButtonModule, IStoreInputFieldModule, IStoreSelectModule, IStoreViewportModule } from '../ui';

import { IStoreAccountComponent } from './account.component';
import { IStoreDeliveryComponent } from './delivery';
import { IStoreFavoritesComponent } from './favorites';
import { IStoreOrdersComponent } from './orders';
import { IStoreSettingsComponent } from './settings';

const generalPath = 'iStore | Account | ';

const route: Route = {
  path: '',
  component: IStoreAccountComponent,
  children: [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: IStoreRoutingPathsEnum.Orders,
    },
    {
      path: IStoreRoutingPathsEnum.Orders,
      component: IStoreOrdersComponent,
      title: `${generalPath}Orders`,
    },
    {
      path: IStoreRoutingPathsEnum.Favorites,
      component: IStoreFavoritesComponent,
      title: `${generalPath}Favorites`,
    },
    {
      path: IStoreRoutingPathsEnum.Delivery,
      component: IStoreDeliveryComponent,
      title: `${generalPath}Delivery`,
    },
    {
      path: IStoreRoutingPathsEnum.Settings,
      component: IStoreSettingsComponent,
      title: `${generalPath}Settings`,
    },
  ],
};

@NgModule({
  declarations: [
    IStoreAccountComponent,
    IStoreDeliveryComponent,
    IStoreFavoritesComponent,
    IStoreOrdersComponent,
    IStoreSettingsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([route]),
    IStoreButtonModule,
    IStoreInputFieldModule,
    IStoreSelectModule,
    IStoreViewportModule,
  ],
  exports: [IStoreAccountComponent],
})
export class IStoreAccountModule { }
