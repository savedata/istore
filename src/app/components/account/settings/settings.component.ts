import { Component } from '@angular/core';
import { FormBuilder, ValidationErrors } from '@angular/forms';
import { BehaviorSubject, catchError, of, Subject, switchMap, takeUntil, tap } from 'rxjs';

import { IStoreUserInterface } from '../../../interfaces';
import { USER_LIST_MOCK } from '../../../mocks';
import { IStoreDestroyService, IStoreUserApiService, IStoreUserService } from '../../../services';

const controlErrors: { [key: string]: string } = {
  emailNotUnique: 'Email is not unique',
  invalidPassword: 'Invalid password',
  required: '{fieldName} is required',
  unknown: 'Unknown error',
};

@Component({
  selector: 'istore-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  providers: [IStoreDestroyService],
})
export class IStoreSettingsComponent {

  public readonly userList = USER_LIST_MOCK;
  public readonly settingsForm = this.formBuilder.group({
    firstName: [''],
    lastName: [''],
    password: [''],
    email: [''],
  });

  public readonly isPhoneForm$ = new BehaviorSubject<boolean>(true);
  private readonly initValidateForm$ = new Subject<void>();
  public readonly validateForm$ = this.initValidateForm$
    .pipe(
      switchMap(() => this.settingsForm.valueChanges),
      tap((value) => {
        console.log(value);
      }));

  public readonly fillUserInfo$ = this.iStoreUserService.currentUser$
    .pipe(
      tap((user) => {
        user && this.settingsForm.patchValue(user);
      }));

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly destroy$: IStoreDestroyService,
    private readonly iStoreUserService: IStoreUserService,
    private readonly iStoreUserApiService: IStoreUserApiService,
  ) { }

  public fillFields(user: Partial<IStoreUserInterface>): void {
    this.settingsForm.patchValue(user);
    this.settingsForm.markAsDirty();
  }

  public getControlError(errors: ValidationErrors | null): string {
    const lastError = Object.keys(errors ?? { unknown: true }).pop();

    return controlErrors[lastError ?? 'unknown'];
  }

  public updateUserInfo(): void {
    const { controls, dirty, valid } = this.settingsForm;
    const user = this.settingsForm.value as IStoreUserInterface;

    if (dirty && valid) {
      this.iStoreUserApiService
        .updateUserInfo(user)
        .pipe(
          tap((user) => {
            this.iStoreUserService.currentUser$.next(user);
          }),
          catchError(error => {
            error?.error?.emailNotUnique && controls.email.setErrors({ emailNotUnique: true });

            return of(null);
          }),
          takeUntil(this.destroy$))
        .subscribe();
    }
  }
}
