import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreViewportComponent } from './viewport.component';

@NgModule({
  declarations: [IStoreViewportComponent],
  imports: [CommonModule],
  exports: [IStoreViewportComponent],
})
export class IStoreViewportModule { }
