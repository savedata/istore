import { Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'istore-viewport',
  templateUrl: './viewport.component.html',
  styleUrls: ['./viewport.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreViewportComponent {
  @HostBinding('class.istore-viewport_show-sidenav')
  @Input() showSidenav: boolean | null = null;

  @HostBinding('class.istore-viewport_show-cart')
  @Input() showCart: boolean | null = null;
}
