import { Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'istore-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreInputFieldComponent {
  @Input() borders = true;
  @Input() errorMessage = '';
  @Input() label = '';

  @HostBinding('class.istore-input-field_invalid')
  @Input() isFieldInvalid = false;
}
