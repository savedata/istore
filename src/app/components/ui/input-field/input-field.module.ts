import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreInputFieldComponent } from './input-field.component';

@NgModule({
  declarations: [IStoreInputFieldComponent],
  imports: [CommonModule],
  exports: [IStoreInputFieldComponent],
})
export class IStoreInputFieldModule { }
