import {
  ConnectionPositionPair,
  OriginConnectionPosition,
  Overlay,
  OverlayConfig,
  OverlayRef,
} from '@angular/cdk/overlay';
import { CdkPortal } from '@angular/cdk/portal';
import { Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { take, tap } from 'rxjs';

export interface IStoreDropdownOffset {
  offsetX: -1 | 0 | 1;
  offsetY: -1 | 0 | 1;
}

@Component({
  selector: 'istore-dropdown',
  template: `
    <ng-template cdkPortal>
      <ng-content></ng-content>
      <div #anchor></div>
    </ng-template>
  `,
  styleUrls: ['dropdown.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreDropdownComponent {
  public isShowing = false;

  @Input() public offset: IStoreDropdownOffset = { offsetX: 0, offsetY: 0 };
  @Input() public reference: HTMLDivElement | undefined = undefined;

  @Output() public loadMore = new EventEmitter<boolean>();
  @Output() public showing = new EventEmitter<boolean>();

  @ViewChild(CdkPortal) public dropdownTemplate: CdkPortal | undefined = undefined;

  constructor(private overlay: Overlay) { }

  private getOverlayConfig(): OverlayConfig {
    const { offsetX, offsetY } = this.offset;
    const originPosition: OriginConnectionPosition = { originX: 'start', originY: 'bottom' };
    const position = this.overlay.position();
    const positionStrategy = !this.reference
      ? position.global().centerHorizontally().centerVertically()
      : position
        .flexibleConnectedTo(this.reference)
        .withPositions([
          new ConnectionPositionPair(originPosition, { overlayX: 'start', overlayY: 'top' }, offsetX, offsetY),
          new ConnectionPositionPair(originPosition, { overlayX: 'start', overlayY: 'bottom' }, offsetX, offsetY),
        ])
        .withPush(false);

    return new OverlayConfig({
      backdropClass: 'istore-backdrop',
      hasBackdrop: true,
      panelClass: 'istore-dropdown__container',
      positionStrategy,
      scrollStrategy: this.overlay.scrollStrategies.block(),
    });
  }

  public show(width: number): IStoreDropdownRef {
    const overlayRef = this.overlay.create(this.getOverlayConfig());
    const dropdownRef = new IStoreDropdownRef(overlayRef);
    const observerCallback = (entries: IntersectionObserverEntry[]): void => {
      for (const entry of entries) {
        entry.target === anchor && entry.isIntersecting && this.loadMore.emit(true);
      }
    };
    const options = { root: overlayRef.overlayElement, rootMargin: '0px', threshold: 1 };
    const observer = new IntersectionObserver(observerCallback, options);

    overlayRef.updateSize({ width });
    overlayRef.attach(this.dropdownTemplate);
    overlayRef
      .backdropClick()
      .pipe(
        take(1),
        tap(() => {
          dropdownRef.close();
          this.isShowing = false;
          this.showing.emit(false);
        }))
      .subscribe();

    const anchor = overlayRef.overlayElement.lastElementChild;
    anchor && observer.observe(anchor);
    this.isShowing = true;
    this.showing.emit(true);

    return dropdownRef;
  }
}

export class IStoreDropdownRef {
  constructor(private overlayRef: OverlayRef) { }

  public close(): void {
    this.overlayRef.dispose();
  }
}
