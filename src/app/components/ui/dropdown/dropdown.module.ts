import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreDropdownComponent } from './dropdown.component';

@NgModule({
  declarations: [IStoreDropdownComponent],
  imports: [CommonModule, PortalModule, OverlayModule, ScrollingModule],
  exports: [IStoreDropdownComponent],
})
export class IStoreDropdownModule { }
