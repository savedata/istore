import { Component, HostBinding, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'istore-digits-field',
  templateUrl: './digits-field.component.html',
  styleUrls: ['./digits-field.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreDigitsFieldComponent {
  @Input() errorMessage = '';
  @Input() label = '';

  @HostBinding('class.istore-digits-field_invalid')
  @Input() isFieldInvalid: boolean | null = false;

  @Input() set maxDigits(digits: number) {
    this.digits.length = Math.abs(digits);
  };

  public readonly digits = [];
}
