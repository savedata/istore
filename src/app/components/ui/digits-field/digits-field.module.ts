import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreDigitsFieldComponent } from './digits-field.component';

@NgModule({
  declarations: [IStoreDigitsFieldComponent],
  imports: [CommonModule],
  exports: [IStoreDigitsFieldComponent],
})
export class IStoreDigitsFieldModule { }
