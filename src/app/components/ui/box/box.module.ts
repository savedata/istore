import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreBoxComponent } from './box.component';

@NgModule({
  declarations: [IStoreBoxComponent],
  imports: [CommonModule],
  exports: [IStoreBoxComponent],
})
export class IStoreBoxModule { }
