import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'istore-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreBoxComponent {
  @Input() label = '';
}
