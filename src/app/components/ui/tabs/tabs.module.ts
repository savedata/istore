import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreTabComponent } from './tab.component';
import { IStoreTabsComponent } from './tabs.component';

const components = [
  IStoreTabComponent,
  IStoreTabsComponent,
];

@NgModule({
  declarations: components,
  imports: [CommonModule],
  exports: components,
})
export class IStoreTabsModule { }
