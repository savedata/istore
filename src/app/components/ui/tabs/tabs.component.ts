import {
  AfterContentChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  QueryList,
} from '@angular/core';

import { IStoreTabComponent } from './tab.component';

@Component({
  selector: 'istore-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IStoreTabsComponent implements AfterContentChecked {
  @ContentChildren(IStoreTabComponent) private readonly iStoreTabList: QueryList<IStoreTabComponent> | undefined;

  public left = '3px';
  public onResize = false;
  public width = '0';

  constructor(private readonly cdr: ChangeDetectorRef) { }

  ngAfterContentChecked(): void {
    if (this.iStoreTabList && this.iStoreTabList.length) {
      this.width = `calc(${(100 / this.iStoreTabList.length)}% - 7px)`;

      this.iStoreTabList.some((iStoreTab, index) => {
        switch (iStoreTab.elementRef.nativeElement.className) {
          case 'istore-tab_active':
            this.left = `calc(${index} * (${this.width} + 8px) + 3px)`;
            this.cdr.detectChanges();

            return true;
          default:
            return false;
        }
      });
    }
  }
}
