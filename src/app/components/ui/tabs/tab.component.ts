import { Component, ElementRef } from '@angular/core';

@Component({
  selector: 'istore-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
})
export class IStoreTabComponent {
  constructor(public elementRef: ElementRef<HTMLElement>) { }
}
