import { ChangeDetectorRef, Directive, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { IStoreSelectComponent } from './select.component';

@Directive({
  selector: 'istore-select[formControlName], istore-select[formControl], istore-select[ngModel]',
  host: { '(changed)': 'onChange($event)', '(blur)': 'onTouched()', '(click)': 'onTouched()' },
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => IStoreSelectDirective),
    multi: true,
  }],
})
export class IStoreSelectDirective implements ControlValueAccessor {
  onChange = (_: any) => { };
  onTouched = () => { };

  constructor(private cdr: ChangeDetectorRef, private host: IStoreSelectComponent) { }

  writeValue(obj: any): void {
    this.host.selected = obj;
    this.host.selectChanged$.next(this.host.selected);
    this.cdr.detectChanges();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.host.disabled = isDisabled;
  }
}
