import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IStoreDropdownModule } from '../dropdown';
import { IStoreOptionsModule } from '../options';

import { IStoreSelectComponent } from './select.component';
import { IStoreSelectDirective } from './select.directive';

const components = [
  IStoreSelectComponent,
  IStoreSelectDirective,
];

@NgModule({
  declarations: components,
  imports: [CommonModule, FormsModule, IStoreDropdownModule, IStoreOptionsModule],
  exports: [...components, IStoreOptionsModule],
})
export class IStoreSelectModule { }
