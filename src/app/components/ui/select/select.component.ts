import {
  AfterContentInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  QueryList,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, startWith, takeUntil, tap } from 'rxjs';

import { IStoreDestroyService } from '../../../services';
import { IStoreDropdownComponent, IStoreDropdownRef } from '../dropdown';
import { IStoreOptionComponent, IStoreOptionParentClass, IStoreOptionService } from '../options';

@Component({
  selector: 'istore-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [IStoreDestroyService, IStoreOptionService],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreSelectComponent extends IStoreOptionParentClass implements AfterContentInit {
  private dropdownRef: IStoreDropdownRef | undefined = undefined;
  private preselected: any;

  public displayText = '';
  public isOpened = false;
  public selected = '';

  public readonly selectChanged$ = new BehaviorSubject<any>(null);

  @ContentChildren(IStoreOptionComponent) public options: QueryList<IStoreOptionComponent> | undefined = undefined;

  @Input() public disabled = false;
  @Input() public errorMessage = '';
  @Input() public hideArrow = false;
  @Input() public isFieldInvalid = false;
  @Input() public label = '';
  @Input() public placeholder = '';
  @Input() public required = false;
  @Input() public selectType: 'label' | null = null;
  @Input('selected') set setSelected(selected: any) {
    this.preselected = selected;
  }

  @Output() public changed = new EventEmitter<any>();

  @ViewChild(IStoreDropdownComponent) public dropdown: IStoreDropdownComponent | undefined = undefined;

  constructor(
    private cdr: ChangeDetectorRef,
    private elementRef: ElementRef<HTMLElement>,
    private iStoreOptionService: IStoreOptionService,
    destroy$: IStoreDestroyService,
  ) {
    super();
    this.iStoreOptionService.registerParent(this);
    this.selectOption$
      .pipe(
        tap(option => {
          this.selected = option.value;
          this.selectedOption = option;
          this.displayText = this.selectedOption ? this.selectedOption.label : '';
          this.changed.emit(this.selected);
          this.hideDropdown();
        }),
        takeUntil(destroy$))
      .subscribe();
  }

  ngAfterContentInit(): void {
    if (this.options) {
      const options = this.options.toArray();
      const optionChanges = (this.options.changes as Observable<IStoreOptionComponent[]>).pipe(startWith(options));

      combineLatest([this.selectChanged$, optionChanges])
        .pipe(
          tap(([value, options]) => {
            this.selectedOption = value !== null ? options.find(option => option.value === value) : undefined;
            this.displayText = this.selectedOption ? this.selectedOption.label : '';
            this.cdr.detectChanges();
          }))
        .subscribe();

      if (this.preselected) {
        const option = options.find(option => option.value === this.preselected || option === this.preselected);
        option && this.selectOption$.next(option);
      }
    }
  }

  public showDropdown(): void {
    if (!this.disabled && this.dropdown) {
      const { width } = this.elementRef.nativeElement.getBoundingClientRect();
      this.dropdownRef = this.dropdown.show(width);
      this.cdr.detectChanges();
    }
  }

  public hideDropdown(): void {
    this.dropdownRef && this.dropdownRef.close();
    this.isOpened = false;
    this.cdr.detectChanges();
  }

  public isShowing(isShowing: boolean): void {
    this.isOpened = isShowing;
  }
}
