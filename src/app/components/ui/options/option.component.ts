import { Component, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

import {
  IStoreOptionActionInterface,
  IStoreOptionIconInterface,
  IStoreOptionInterface,
  IStoreOptionParentComponentIntefrace,
} from './option.interface';
import { IStoreOptionService } from './option.service';

@Component({
  selector: 'istore-option',
  templateUrl: 'option.component.html',
  styleUrls: ['option.component.scss'],
})
export class IStoreOptionComponent implements IStoreOptionInterface {
  private parentComponent: IStoreOptionParentComponentIntefrace | undefined = undefined;

  @Input() public action: IStoreOptionActionInterface | undefined = undefined;
  @Input() public icon: IStoreOptionIconInterface | undefined = undefined;
  @Input() public label = '';
  @Input() public value: any = '';

  @Output() public callAction = new EventEmitter<IStoreOptionActionInterface>();

  @HostBinding('class.istore-option__selected') public get selected(): boolean {
    return this.parentComponent !== undefined && this.parentComponent.selectedOption === this;
  }

  constructor(private parentService: IStoreOptionService) {
    this.parentComponent = this.parentService.getParent();
  }

  public actionClick(event: Event): void {
    event.stopPropagation();
    this.action && this.callAction.emit({ id: this.action.id, title: this.action.title, value: this.value });
  }

  @HostListener('click', ['$event']) public onClick(event: UIEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.parentComponent && this.parentComponent.selectOption$.next(this);
  }
}
