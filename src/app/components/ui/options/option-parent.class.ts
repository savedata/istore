import { Subject } from 'rxjs';

import { IStoreOptionInterface, IStoreOptionParentComponentIntefrace } from './option.interface';

export class IStoreOptionParentClass implements IStoreOptionParentComponentIntefrace {
  public selectedOption: IStoreOptionInterface<any> | undefined = undefined;
  public readonly selectOption$ = new Subject<IStoreOptionInterface<any>>();
}
