import { Injectable } from '@angular/core';

import { IStoreOptionParentComponentIntefrace, IStoreOptionParentServiceInterface } from './option.interface';

@Injectable({ providedIn: 'any' })
export class IStoreOptionService implements IStoreOptionParentServiceInterface {
  private select: IStoreOptionParentComponentIntefrace | undefined = undefined;

  public getParent(): IStoreOptionParentComponentIntefrace | undefined {
    return this.select;
  }

  public registerParent(parent: IStoreOptionParentComponentIntefrace): void {
    this.select = parent;
  }
}
