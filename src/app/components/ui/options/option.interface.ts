import { Subject } from 'rxjs';

export interface IStoreOptionInterface<T = any> {
  action?: IStoreOptionActionInterface;
  icon?: IStoreOptionIconInterface;
  label: string;
  value: T;
}

export interface IStoreOptionActionInterface {
  id: string;
  title: string;
  value: any;
}

export interface IStoreOptionIconInterface {
  id: string;
  color: string;
}

export interface IStoreOptionParentComponentIntefrace {
  selectedOption: IStoreOptionInterface | undefined;
  selectOption$: Subject<IStoreOptionInterface<any>>;
}

export interface IStoreOptionParentServiceInterface {
  getParent: () => IStoreOptionParentComponentIntefrace | undefined;
  registerParent: (parent: IStoreOptionParentComponentIntefrace) => void;
}
