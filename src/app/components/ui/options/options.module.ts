import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { IStorePipesModule } from '@istore/pipes';

import { IStoreOptionComponent } from './option.component';

@NgModule({
  declarations: [IStoreOptionComponent],
  imports: [CommonModule, IStorePipesModule],
  exports: [IStoreOptionComponent],
})
export class IStoreOptionsModule { }
