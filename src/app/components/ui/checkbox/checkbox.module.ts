import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreCheckboxComponent } from './checkbox.component';
import { IStoreCheckboxDirective } from './checkbox.directive';

const components = [
  IStoreCheckboxComponent,
  IStoreCheckboxDirective,
];

@NgModule({
  declarations: components,
  imports: [CommonModule],
  exports: components,
})
export class IStoreCheckboxModule { }
