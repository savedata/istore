import { Component, ContentChild, ElementRef, Input, ViewEncapsulation } from '@angular/core';

import { IStoreCheckboxDirective } from './checkbox.directive';

@Component({
  selector: 'istore-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreCheckboxComponent {
  @ContentChild(IStoreCheckboxDirective, { read: ElementRef, static: true }) input!: ElementRef<HTMLInputElement>;

  @Input() label = 'Checkbox label';

  change(): void {
    this.input.nativeElement.click();
  }
}
