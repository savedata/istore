import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'istore-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreListComponent implements OnDestroy, OnInit {
  @Input() loading: boolean | null = false;
  @Output() loadMore = new EventEmitter<number>();

  @ViewChild('anchor', { static: true }) private readonly anchor: ElementRef<HTMLDivElement> | undefined;
  @ViewChild('scrollPort', { static: true }) private readonly scrollPort: ElementRef<HTMLDivElement> | undefined;

  private readonly mapping: Map<Element, Function> = new Map();
  private observer = new IntersectionObserver(() => { });

  ngOnDestroy(): void {
    this.mapping.clear();
    this.observer.disconnect();
  }

  ngOnInit(): void {
    const anchorCallback = (intersected: boolean) => {
      const scrollPortHeight = this.scrollPort?.nativeElement.getBoundingClientRect().height;
      const perPage = scrollPortHeight ? Math.ceil(scrollPortHeight / 50 * 1.25) : 20;

      intersected && this.loadMore.emit(perPage);
    };

    const observerCallback = (entries: IntersectionObserverEntry[]) => {
      for (const entry of entries) {
        const callback = this.mapping.get(entry.target);
        callback && callback(entry.isIntersecting);
      }
    };

    const options = {
      root: this.scrollPort?.nativeElement,
      rootMargin: '50px',
      threshold: 0,
      // rootMargin: '300px',
      // threshold: [0, 1],
    };

    this.observer = new IntersectionObserver(observerCallback, options);
    this.anchor && this.add(this.anchor.nativeElement, anchorCallback);
  }

  private add(element: Element, callback: Function): void {
    this.mapping.set(element, callback);
    this.observer.observe(element);
  }

  private remove(element: Element): void {
    this.mapping.delete(element);
    this.observer.unobserve(element);
  }
}
