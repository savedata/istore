import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'istore-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreListItemComponent { }
