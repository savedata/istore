import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreListComponent } from './list.component';
import { IStoreListItemComponent } from './list-item.component';

const components = [
  IStoreListComponent,
  IStoreListItemComponent,
];

@NgModule({
  declarations: components,
  imports: [CommonModule],
  exports: components,
})
export class IStoreListModule { }
