import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'istore-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreButtonComponent {

}
