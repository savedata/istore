import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IStoreButtonComponent } from './button.component';

@NgModule({
  declarations: [IStoreButtonComponent],
  imports: [CommonModule],
  exports: [IStoreButtonComponent],
})
export class IStoreButtonModule { }
