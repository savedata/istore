import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IStoreSwitcherComponent } from './switcher.component';

@NgModule({
  declarations: [IStoreSwitcherComponent],
  imports: [CommonModule],
  exports: [IStoreSwitcherComponent],
})
export class IStoreSwitcherModule { }
