import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'istore-switcher',
  templateUrl: './switcher.component.html',
  styleUrls: ['./switcher.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreSwitcherComponent { }
