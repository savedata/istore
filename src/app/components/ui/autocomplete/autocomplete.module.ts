import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IStorePipesModule } from '@istore/pipes';

import { IStoreDropdownModule } from '../dropdown';
import { IStoreInputFieldModule } from '../input-field';
import { IStoreOptionsModule } from '../options';

import { IStoreAutocompleteComponent } from './autocomplete.component';
import { IStoreAutocompleteDirective } from './autocomplete.directive';

const components = [
  IStoreAutocompleteComponent,
  IStoreAutocompleteDirective,
];

@NgModule({
  declarations: components,
  exports: components,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IStoreDropdownModule,
    IStoreInputFieldModule,
    IStoreOptionsModule,
    IStorePipesModule,
  ],
})
export class IStoreAutocompleteModule { }
