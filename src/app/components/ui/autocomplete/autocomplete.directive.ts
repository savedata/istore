import { ChangeDetectorRef, Directive, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { IStoreOptionInterface } from '../options';

import { IStoreAutocompleteComponent } from './autocomplete.component';

@Directive({
  selector: 'istore-autocomplete[formControlName], istore-autocomplete[formControl], istore-autocomplete[ngModel]',
  host: { '(changed)': 'onChange($event)', '(blur)': 'onTouched()', '(click)': 'onTouched()' },
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => IStoreAutocompleteDirective),
    multi: true,
  }],
})
export class IStoreAutocompleteDirective implements ControlValueAccessor {
  onTouched = () => { };

  constructor(private cdr: ChangeDetectorRef, private host: IStoreAutocompleteComponent) { }

  writeValue(items: IStoreOptionInterface<any>[]): void {
    this.host.selectedItems = items;
    this.cdr.detectChanges();
  }

  registerOnChange(fn: any): void {
    this.host.change = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.host.disabled = isDisabled;
  }
}
