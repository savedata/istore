import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { map, merge, startWith, Subject, takeUntil, tap } from 'rxjs';

import { IStoreDestroyService } from '@istore/services';

import { IStoreDropdownComponent, IStoreDropdownOffset, IStoreDropdownRef } from '../dropdown';
import { IStoreOptionInterface, IStoreOptionParentClass, IStoreOptionService } from '../options';

@Component({
  selector: 'istore-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
  providers: [IStoreDestroyService, IStoreOptionService],
  encapsulation: ViewEncapsulation.None,
})
export class IStoreAutocompleteComponent extends IStoreOptionParentClass implements OnChanges {
  private dropdownRef: IStoreDropdownRef | undefined = undefined;
  public readonly dropdownOffset: IStoreDropdownOffset = { offsetX: -1, offsetY: 1 };
  public readonly inputField = new FormControl('');
  public selectedItems: IStoreOptionInterface<any>[] = [];

  @Input() public disabled = false;
  @Input() public errorMessage = '';
  @Input() public isFieldInvalid = false;
  @Input() public label = '';
  @Input() public loading = false;
  @Input() public placeholder = '';
  @Input() public items: IStoreOptionInterface[] | null = [];

  @Output() public readonly filterChanged = new EventEmitter<string>();
  @Output() public readonly loadMore = new EventEmitter<boolean>();

  @ViewChild('input', { static: true }) input: ElementRef<HTMLInputElement> | undefined = undefined;
  @ViewChild(IStoreDropdownComponent) public dropdown: IStoreDropdownComponent | undefined = undefined;

  private readonly refreshValue$ = new Subject<any>();
  private readonly filterChanged$ = this.inputField
    .valueChanges
    .pipe(
      tap(value => {
        this.filterChanged.emit(value ?? '');
      }));

  public readonly filteredItems$ = merge(
    this.filterChanged$,
    this.refreshValue$,
  ).pipe(
    startWith(''),
    map(value => this.filter(value)));

  constructor(
    private cdr: ChangeDetectorRef,
    private elementRef: ElementRef<HTMLElement>,
    private iStoreOptionService: IStoreOptionService,
    readonly destroy$: IStoreDestroyService,
  ) {
    super();
    this.iStoreOptionService.registerParent(this);
    this.selectOption$
      .pipe(
        tap(option => {
          this.input && this.input.nativeElement.blur();
          this.selectedItems.push(option);
          this.change(this.selectedItems);
          this.hideDropdown();
        }),
        takeUntil(destroy$))
      .subscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    changes['items'] && this.refreshValue$.next(this.inputField.value);
  }

  public change = (_: any) => { }

  private filter(value: string | any): IStoreOptionInterface[] {
    const normalizeValue = (value: string): string => value?.toLowerCase().replace(/\s/g, '') ?? '';
    const filterValue = normalizeValue(value.title ?? value);

    return this.items
      ? this.items.filter(item =>
          !this.selectedItems.some(selectedItem => selectedItem.label === item.label)
          && normalizeValue(item.label).includes(filterValue))
      : [];
  }

  public resetFilter(): void {
    this.inputField.patchValue('');
    this.dropdown && this.dropdown.isShowing && this.input && this.input.nativeElement.focus();
  }

  public showDropdown(): void {
    if (!this.disabled && this.dropdown && !this.dropdown.isShowing) {
      this.input && this.input.nativeElement.focus();
      this.refreshValue$.next(this.inputField.value);
      const { width } = this.elementRef.nativeElement.getBoundingClientRect();
      this.dropdownRef = this.dropdown.show(width);
      this.cdr.detectChanges();
    }
  }

  public hideDropdown(): void {
    this.dropdown && (this.dropdown.isShowing = false);
    this.dropdownRef && this.dropdownRef.close();
    this.cdr.detectChanges();
  }

  public setFocus(): void {
    this.filterChanged.emit(this.inputField.value ?? '');
  }

  public removeItem(index: number): void {
    this.selectedItems.splice(index, 1);
    this.change(this.selectedItems);
  }

  public readonly trackOption = (index: number, option: IStoreOptionInterface) => option;
}
