export enum IStoreSortingOrderByEnum {
  All = 'all',
  ActiveStatus = 'active',
  Description = 'description',
  Price = 'price',
  Quantity = 'quantity',
  SKU = 'sku',
  Slug = 'slug',
  Tite = 'title',
  UpdatedAt = 'updatedAt',
}
