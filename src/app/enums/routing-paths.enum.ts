export enum IStoreRoutingPathsEnum {
  // General paths
  Catalog = 'catalog',
  Contacts = 'contacts',
  Main = '',
  NotFound = 'not-found',

  // Account paths
  Account = 'account',
  Delivery = 'delivery',
  Favorites = 'favorites',
  Orders = 'orders',
  Settings = 'settings',

  // Admin paths
  Admin = 'admin-panel',
  Categories = 'categories',
  DeliveryMethods = 'delivery-methods',
  PaymentMethods = 'payment-methods',
  Prices = 'prices',
  Products = 'products',
  Sale = 'sale',
  Statuses = 'statuses',
  Users = 'users',

  // Reusable admin paths
  Editor = 'editor',
  EntityEditor = 'editor/:entityId',
  EntityList = 'list',

  // Auth paths
  Auth = 'auth',
  ForgotPassword = 'forgot-password',
  SignIn = 'signin',
  SignUp = 'signup',
}
