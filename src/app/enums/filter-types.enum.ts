export enum IStoreFilterTypesEnum {
  Date = 'date',
  Number = 'number',
  Options = 'options',
  String = 'string',
  Time = 'time',
  Boolean = 'boolean',
}
