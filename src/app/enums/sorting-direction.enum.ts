export enum IStoreSortingDirectionEnum {
  Ascending = 'asc',
  Descending = 'desc',
}

export enum IStoreSortingDirectionArrowEnum {
  Up = '\u25b2',
  Down = '\u25bc',
}
