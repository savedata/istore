export enum IStoreUserRoles {
  Admin = 'ROLE_ADMIN',
  Moderator = 'ROLE_MODERATOR',
  User = 'ROLE_USER',
}
