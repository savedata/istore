import { animate, group, query, style, transition, trigger } from '@angular/animations';

import { IStoreRoutingPathsEnum } from './enums';

const animationTiming = '0.7s ease-in-out';
const backLayerOnSliderHidden = { filter: 'none', overflow: 'hidden' };
const backLayerOnSliderInSight = { filter: 'blur(10px)', overflow: 'hidden' };
const sliderHidden = { 'box-shadow': 'none', transform: 'translateY(110%)' };
const sliderInSight = { 'box-shadow': '0 0 12px 0 rgba(0, 0, 0, 0.1)', transform: 'translateY(0)', zIndex: 1 };
const sliderConfig = {
  position: 'fixed',
  top: '60px',
  bottom: '0px',
  left: '0',
  right: '0',
  margin: '10px auto 0',
  'background': '#fff',
  'border-top-left-radius': '12px',
  'border-top-right-radius': '12px',
  'box-shadow': '0 0 12px 0 rgba(0, 0, 0, 0.1)',
};

const transitionList = [IStoreRoutingPathsEnum.Account, IStoreRoutingPathsEnum.Admin];
const transitions = (reverseTransition: boolean, ...routes: IStoreRoutingPathsEnum[]) => routes
  .map(route => reverseTransition ? `${route} => *` : `* => ${route}`)
  .join(', ');

const ANY_TO_SLIDER = transition(
  transitions(false, ...transitionList),
  [
    query(':enter', style(sliderConfig), { optional: true }),
    group([
      query(':enter', [
        style(sliderHidden),
        animate(animationTiming, style(sliderInSight)),
      ], { optional: true }),
      query(':leave *', [
        style(backLayerOnSliderHidden),
        animate(animationTiming, style(backLayerOnSliderInSight)),
      ], { optional: true }),
    ]),
  ],
);

const SLIDER_TO_ANY = transition(
  transitions(true, ...transitionList),
  [
    query(':leave', style(sliderConfig), { optional: true }),
    group([
      query(':leave', [
        style(sliderInSight),
        animate(animationTiming, style(sliderHidden)),
      ], { optional: true }),
      query(':enter *', [
        style(backLayerOnSliderInSight),
        animate(animationTiming, style(backLayerOnSliderHidden)),
      ], { optional: true }),
    ]),
  ],
);

export const APP_ANIMATIONS = [
  trigger('appAnimation', [
    ANY_TO_SLIDER,
    SLIDER_TO_ANY,
  ]),
];
