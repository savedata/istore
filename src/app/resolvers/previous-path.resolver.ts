import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';

import { IStoreRoutingPathsEnum } from '../enums';

const DEFAULT_URL = '/' + IStoreRoutingPathsEnum.Catalog;
const PATHS_TO_EXCEPT = [
  IStoreRoutingPathsEnum.Account,
  IStoreRoutingPathsEnum.Admin,
  IStoreRoutingPathsEnum.Auth,
  IStoreRoutingPathsEnum.NotFound,
];

@Injectable({ providedIn: 'root' })
export class IStorePreviousPathResolver implements Resolve<string> {
  constructor(private readonly router: Router) { }

  resolve(): string {
    const previousUrl = this.router.getCurrentNavigation()?.previousNavigation?.finalUrl?.toString();
    const isPreviousUrlValid = (previousUrl: string) => !PATHS_TO_EXCEPT.some(path => previousUrl.includes(path));

    return previousUrl && isPreviousUrlValid(previousUrl) ? previousUrl : DEFAULT_URL;
  }
}
