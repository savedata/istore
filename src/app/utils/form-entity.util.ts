import { FormGroup } from '@angular/forms';

export namespace FormEntity {
  export function getFromDirtyFields<T extends { id?: number | undefined }>(form: FormGroup): T {
    const { controls, value } = form;

    return !value.id
      ? value as T
      : Object.entries(controls).reduce((entity, [controlName, control]) => {
          control.dirty && Object.assign(entity, { [controlName]: control.value });

          return entity;
        }, { id: value.id } as T);
  }
}
