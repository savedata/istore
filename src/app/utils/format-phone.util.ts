import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

const PHONE_MASK = '+7(***)-***-**-**';

export namespace PhoneNumber {
  export function setFormat(phone: string | null): string {
    const maskedPhoneNumber = (phone || '')
      .replace(/^\+7/, '')
      .replace(/[^0-9]/g, '')
      .split('')
      .reduce((phoneNumber, char) => phoneNumber.replace('*', char), PHONE_MASK);

    const maskIndex = maskedPhoneNumber.indexOf('*');
    const filledPhoneNumber = maskIndex >= 0 ? maskedPhoneNumber.substring(0, maskIndex) : maskedPhoneNumber;

    return filledPhoneNumber.trim().replace(/((\+7\()?([)-]{1,2})?)$/,'');
  }

  export function cleanFormat(phone: string | null | undefined): string {
    return (phone || '').replace(/[^0-9]/g, '');
  }

  export function validator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const valid = control.value.length < 1 || control.value.length >= 17;

      return valid ? null : { incorrectPhoneNumber: true };
    }
  }
}
