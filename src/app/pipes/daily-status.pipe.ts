import { Pipe, PipeTransform } from '@angular/core';

enum IStoreDailyStatusEnum {
  Seconds = 'seconds ago',
  Minutes = 'minutes ago',
  Hours = 'hours ago',
  Days = 'days ago',
  Months = 'months ago',
  Years = 'years ago',
}

const DAILY_STATUSES_IN_SECONDS = {
  [IStoreDailyStatusEnum.Years]: 365 * 24 * 60 * 60 * 60,
  [IStoreDailyStatusEnum.Months]: 30 * 24 * 60 * 60,
  [IStoreDailyStatusEnum.Days]: 24 * 60 * 60,
  [IStoreDailyStatusEnum.Hours]: 60 * 60,
  [IStoreDailyStatusEnum.Minutes]: 60,
  [IStoreDailyStatusEnum.Seconds]: 1,
};

const message = (range: number, description: string) => `${Math.floor(range)} ${description}`;

@Pipe({ name: 'dailystatus' })
export class IStoreDailyStatusPipe implements PipeTransform {
  transform(value: string | undefined): string | undefined {
    if (value) {
      const now = new Date();
      const targetDate = new Date(value);
      const timeRangeInSeconds = (now.getTime() - targetDate.getTime()) / 1000;

      const [status = IStoreDailyStatusEnum.Seconds, seconds = 1] = Object
        .entries(DAILY_STATUSES_IN_SECONDS)
        .find(([status, seconds]) => seconds < timeRangeInSeconds) ?? [];

      return message(timeRangeInSeconds / seconds, status);
    } else {
      return value;
    }
  }
}
