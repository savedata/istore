import { ElementRef, Pipe, PipeTransform } from '@angular/core';

import { IStoreMediaService } from '../services';

@Pipe({ name: 'mediaurl' })
export class IStoreMediaUrlPipe implements PipeTransform {
  constructor(
    private readonly elementRef: ElementRef<HTMLImageElement>,
    private readonly iStoreMediaService: IStoreMediaService,
  ) { }

  transform(image: string | null | undefined): string | null | undefined {
    const { width, height } = this.elementRef.nativeElement.getBoundingClientRect();

    return image ? this.iStoreMediaService.getMediaUrl(image) : 'assets/product-placeholder.jpg';
  }
}
