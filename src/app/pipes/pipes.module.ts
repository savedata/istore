import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { IStoreDailyStatusPipe } from './daily-status.pipe';
import { IStoreMediaUrlPipe } from './media-url.pipe';

const pipes = [IStoreDailyStatusPipe, IStoreMediaUrlPipe];

@NgModule({
  declarations: pipes,
  imports: [CommonModule],
  exports: pipes,
})
export class IStorePipesModule { }
