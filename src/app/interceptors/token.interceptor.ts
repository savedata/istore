import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ng2-cookies';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class IStoreTokenInterceptor implements HttpInterceptor {
  constructor (private cookieService: CookieService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        'x-access-token': this.cookieService.get('access_token'),
      },
    });

    return next.handle(request);
  }
}
