import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IStoreRoutingPathsEnum } from './enums';
import { IStoreAccessGuard } from './guards';
import { IStorePreviousPathResolver } from './resolvers';

const generalPath = 'iStore | ';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: IStoreRoutingPathsEnum.Catalog,
  },
  {
    path: IStoreRoutingPathsEnum.Admin,
    loadChildren: () => import('./components/admin').then(m => m.IStoreAdminModule),
    canLoad: [IStoreAccessGuard],
    data: {
      acceptForUser: true,
      checkAdminRole: true,
      animation: IStoreRoutingPathsEnum.Admin,
      customRedirect: IStoreRoutingPathsEnum.NotFound,
    },
    resolve: {
      previousPath: IStorePreviousPathResolver,
    },
    title: `${generalPath}Admin`,
  },
  {
    path: IStoreRoutingPathsEnum.Account,
    loadChildren: () => import('./components/account').then(m => m.IStoreAccountModule),
    canActivate: [IStoreAccessGuard],
    data: {
      acceptForUser: true,
      animation: IStoreRoutingPathsEnum.Account,
      customRedirect: IStoreRoutingPathsEnum.Auth,
    },
    resolve: {
      previousPath: IStorePreviousPathResolver,
    },
    title: `${generalPath}Account`,
  },
  {
    path: IStoreRoutingPathsEnum.Auth,
    loadChildren: () => import('./components/auth').then(m => m.IStoreAuthModule),
    canActivate: [IStoreAccessGuard],
    data: {
      acceptForUser: false,
      customRedirect: IStoreRoutingPathsEnum.Account,
    },
    title: `${generalPath}Auth`,
  },
  {
    path: IStoreRoutingPathsEnum.Catalog,
    loadChildren: () => import('./components/catalog').then(m => m.IStoreCatalogModule),
    data: {
      animation: IStoreRoutingPathsEnum.Catalog,
    },
    title: `${generalPath}Catalog`,
  },
  {
    path: IStoreRoutingPathsEnum.Contacts,
    loadChildren: () => import('./components/contacts').then(m => m.IStoreContactsModule),
    data: {
      animation: IStoreRoutingPathsEnum.Contacts,
    },
    title: `${generalPath}Contacts`,
  },
  {
    path: IStoreRoutingPathsEnum.NotFound,
    loadChildren: () => import('./components/not-found').then(m => m.IStoreNotFoundModule),
    title: `${generalPath}Not found`,
  },
  {
    path: '**',
    redirectTo: IStoreRoutingPathsEnum.NotFound,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [IStoreAccessGuard],
})
export class AppRoutingModule { }
