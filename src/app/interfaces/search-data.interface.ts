import { IStoreSortingDirectionEnum } from '../enums';

import { IStoreSearchFiltersInterface } from './search-filters.interface';

export interface IStoreSearchDataInterface {
  configuration?: IStoreSearchFiltersInterface;
  direction: IStoreSortingDirectionEnum;
  orderBy: string;
  page: number;
  perPage: number;
  total: number;
  skip?: number[];
}
