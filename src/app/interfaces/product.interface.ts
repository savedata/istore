export interface IStoreProductInterface {
  id?: number;
  active: boolean;
  brand: string;
  categories: number[];
  description: string;
  title: string;
  sku: string;
  slug: string;
  quantity: number;
}
