import { IStoreSortingDirectionEnum, IStoreSortingOrderByEnum } from '../enums';

export interface IStoreSortingOrderInterface {
  direction: IStoreSortingDirectionEnum;
  orderBy: IStoreSortingOrderByEnum;
}
