import { IStoreSortingDirectionArrowEnum, IStoreSortingDirectionEnum, IStoreSortingOrderByEnum } from '../enums';

export interface IStoreListFieldInterface {
  id: IStoreSortingOrderByEnum;
  label: string;
  arrow: {
    asc: IStoreSortingDirectionArrowEnum;
    desc: IStoreSortingDirectionArrowEnum;
  };
  defaultSortDirection: IStoreSortingDirectionEnum;
}
