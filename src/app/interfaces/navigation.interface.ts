import { IStoreRoutingPathsEnum } from '../enums';

export interface IStoreNavigationInterface {
  path: IStoreRoutingPathsEnum[];
  title: string;
}

export interface IStoreNavigationListInterface {
  [pathName: string]: IStoreNavigationInterface;
}
