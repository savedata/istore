export interface IStoreMediaInterface {
  id: number;
  path: string;
  default?: boolean;
  categoryId: number | null;
  productId: number | null;
  userId: number | null;
}
