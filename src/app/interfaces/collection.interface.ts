import { IStorePaginationInterface } from './pagination.interface';

export interface IStoreCollectionInterface<T> {
  collection: T[];
  pagination: IStorePaginationInterface;
}
