import { IStoreUserRoles } from '../enums';

export interface IStoreUserInterface {
  accessToken: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  phoneNumber: string;
  roles: IStoreUserRoles[] | string[];
}
