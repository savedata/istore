export interface IStoreConfirmationInterface {
  confirmationCode: string | null;
  phoneNumber: string;
}

export interface IStoreRegistrationInterface {
  phoneNumber: string;
}
