import { IStoreFilterConditionsEnum } from '../enums';

export interface IStoreSearchFilterInterface {
  condition: IStoreFilterConditionsEnum;
  value: any;
}

export interface IStoreSearchFiltersInterface {
  [propName: string]: IStoreSearchFilterInterface[];
}

export interface IStorePropertyFilterInterface {
  name: string;
  filter: IStoreSearchFilterInterface;
}
