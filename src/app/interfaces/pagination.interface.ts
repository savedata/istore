export interface IStorePaginationInterface {
  count: number;
  page: number;
  perPage: number;
  total: number;
}
