import { IStoreMediaInterface } from './media.interface';

export interface IStoreCategoryInterface {
  id?: number;
  active: boolean;
  description: string;
  defaultImage: string | null;
  media?: number[] | Partial<IStoreMediaInterface>[];
  title: string;
  slug: string;
  updatedAt?: string;
}
