export interface IStoreCredentialsInterface {
  password: string;
  phoneNumber: string;
}
