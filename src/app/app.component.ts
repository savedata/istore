import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { APP_ANIMATIONS } from './app-animations';

@Component({
  selector: 'istore-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: APP_ANIMATIONS,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  public getAnimationData(outlet: RouterOutlet) {
    // console.log(outlet.activatedRouteData['animation']);
    return outlet.activatedRouteData['animation'];
  }
}
