import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CookieService } from 'ng2-cookies';
import { of, tap } from 'rxjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IStoreFooterModule } from './components/footer';
import { IStoreHeaderModule } from './components/header';
import { IStoreTokenInterceptor } from './interceptors';
import { IStoreAuthService, IStoreUserApiService, IStoreUserService } from './services';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,

    IStoreFooterModule,
    IStoreHeaderModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    CookieService,
    {
      deps: [CookieService],
      provide: HTTP_INTERCEPTORS,
      useExisting: IStoreTokenInterceptor,
      multi: true,
    }
  ],
})
export class AppModule {
  constructor(
    iStoreAuthService: IStoreAuthService,
    iStoreUserService: IStoreUserService,
    iStoreUserApiService: IStoreUserApiService,
  ) {
    const userInfo$ = iStoreAuthService.checkAccessToken()
      ? iStoreUserApiService.getUserInfo()
      : of(null);

    userInfo$
      .pipe(
        tap(user => {
          iStoreUserService.currentUser$.next(user);
        }))
      .subscribe();
  }
}
