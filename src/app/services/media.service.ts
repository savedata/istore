import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { mergeMap, Observable, of, switchMap } from 'rxjs';

import { environment } from '../environments';
import { IStoreMediaInterface } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class IStoreMediaService {
  constructor(private readonly httpClient: HttpClient) { }

  private get mediaApiPath(): string {
    return environment.backend + '/media';
  }

  public removeFile(fileId: number): Observable<void> {
    return this.httpClient.delete<void>(`${this.mediaApiPath}/${fileId}`);
  }

  public getMediaUrl(filePath: string): string {
    return `${this.mediaApiPath}/${filePath}`;
  }

  public setDefaultImage(media: IStoreMediaInterface): Observable<IStoreMediaInterface[]> {
    return this.httpClient.post<IStoreMediaInterface[]>(`${this.mediaApiPath}/default`, media);
  }

  public uploadFiles<T>(files: File[]): Observable<HttpEvent<T>> {
    const formData = new FormData();

    return of(files)
      .pipe(
        mergeMap(async (fileList) => {
          for (const file of fileList) {
            formData.append('images', file);
          }

          return formData;
        }),
        switchMap(formData => this.httpClient
          .post<any>(this.mediaApiPath, formData, { observe: 'events', reportProgress: true })));
  }
}
