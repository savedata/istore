import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { IStoreFilterConditionsEnum } from '../enums';
import { environment } from '../environments';
import {
  IStoreCategoryInterface,
  IStoreCollectionInterface,
  IStoreProductInterface,
  IStoreSearchDataInterface,
} from '../interfaces';

@Injectable({ providedIn: 'root' })
export class IStoreProductApiService {

  constructor(private httpClient: HttpClient) { }

  private get apiPath(): string {
    return environment.backend;
  }

  private get categoryPath(): string {
    return `${this.apiPath}/category`;
  }

  private get productPath(): string {
    return `${this.apiPath}/product`;
  }

  // categories
  public createCategory({ id, ...category }: IStoreCategoryInterface): Observable<IStoreCategoryInterface> {
    return this.httpClient.post<IStoreCategoryInterface>(this.categoryPath, category);
  }

  public getCategoryList(
    searchData: IStoreSearchDataInterface,
  ): Observable<IStoreCollectionInterface<IStoreCategoryInterface>> {
    const params = this.getSearchParams(searchData);

    return this.httpClient
      .get<IStoreCollectionInterface<IStoreCategoryInterface>>(`${this.categoryPath}/all`, { params });
  }

  public getCategory(categoryId: number): Observable<IStoreCategoryInterface> {
    return this.httpClient.get<IStoreCategoryInterface>(`${this.categoryPath}/${categoryId}`);
  }

  public updateCategory({ id, ...category }: Partial<IStoreCategoryInterface>): Observable<IStoreCategoryInterface> {
    return this.httpClient.patch<IStoreCategoryInterface>(`${this.categoryPath}/${id}`, category);
  }

  public deleteCategory(categoryId: number): Observable<void> {
    return this.httpClient.delete<void>(`${this.categoryPath}/${categoryId}`);
  }

  // products
  public createProduct({ id, ...product }: IStoreProductInterface): Observable<IStoreProductInterface> {
    return this.httpClient.post<IStoreProductInterface>(this.productPath, product);
  }

  public getProductList(): Observable<IStoreProductInterface[]> {
    return this.httpClient.get<IStoreProductInterface[]>(`${this.productPath}/all`);
  }

  public getProduct(productId: number): Observable<IStoreProductInterface> {
    return this.httpClient.get<IStoreProductInterface>(`${this.productPath}/${productId}`);
  }

  public updateProduct({ id, ...product }: Partial<IStoreProductInterface>): Observable<IStoreProductInterface> {
    return this.httpClient.patch<IStoreProductInterface>(`${this.productPath}/${id}`, product);
  }

  public deleteProduct(productId: number): Observable<void> {
    return this.httpClient.delete<void>(`${this.productPath}/${productId}`);
  }

  private getSearchParams(searchData: IStoreSearchDataInterface): HttpParams {
    let searchParams = new HttpParams()
      .set('orderBy', searchData.orderBy ? searchData.orderBy : 'updatedAt')
      .set('direction', searchData.direction ? searchData.direction : 'asc')
      .set('limit', searchData.perPage ? searchData.perPage : 20)
      .set('page', searchData.page ? searchData.page : 1)
      .set('skip', searchData.skip ? `${searchData.skip}` : 0)

    Object.entries(searchData.configuration ?? { }).forEach(([propName, filters], propIndex) => {
      const sortedFilters = filters.reduce((conditions, { condition, value }) => {
        if (condition in conditions) {
          conditions[condition].push(value);
        } else {
          Object.assign(conditions, { [condition]: [value] });
        }

        return conditions;
      }, { } as { [condition: string]: any[] });

      Object.entries(sortedFilters).forEach(([condition, values], conditionIndex) => {
        if (condition === IStoreFilterConditionsEnum.BetweenDates) {
          values = values.join().split(',');
        }

        values.forEach((value, index) => {
          searchParams = searchParams
            .set(`filters[${propName}][${conditionIndex}][condition]`, condition)
            .set(`filters[${propName}][${conditionIndex}][values][${index}]`, value);
        });
      });
    });

    return searchParams;
  }
}
