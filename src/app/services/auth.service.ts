import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ng2-cookies';
import { Observable, tap } from 'rxjs';

import { IStoreRoutingPathsEnum } from '../enums';
import { environment } from '../environments';
import {
  IStoreConfirmationInterface,
  IStoreCredentialsInterface,
  IStoreRegistrationInterface,
  IStoreUserInterface,
} from '../interfaces';

const ACCESS_TOKEN = 'access_token';

@Injectable({ providedIn: 'root' })
export class IStoreAuthService {

  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient,
  ) { }

  private get apiPath(): string {
    return environment.backend;
  }

  private get authPath(): string {
    return `${this.apiPath}/auth`;
  }

  public checkAccessToken(): boolean {
    return this.cookieService.check(ACCESS_TOKEN);
  }

  public confirmPhoneNumber(confirmationData: IStoreConfirmationInterface): Observable<IStoreUserInterface> {
    return this.httpClient
      .post<IStoreUserInterface>(`${this.authPath}/confirm-phone`, confirmationData)
      .pipe(tap(this.setToken));
  }

  public getConfirmationCode(registrationData: IStoreRegistrationInterface): Observable<any> {
    return this.httpClient.post(`${this.authPath}/confirm-code`, registrationData);
  }

  public logout(): void {
    Object.values(IStoreRoutingPathsEnum).forEach(path => {
      this.cookieService.delete(ACCESS_TOKEN, `/${path}`);
    });
  }

  public singIn(credentials: IStoreCredentialsInterface): Observable<IStoreUserInterface> {
    return this.httpClient
      .post<IStoreUserInterface>(`${this.authPath}/signin`, credentials)
      .pipe(tap(this.setToken));
  }

  private setToken = (user: IStoreUserInterface): void => {
    this.cookieService.set(ACCESS_TOKEN, user.accessToken, 86400, '/');
  }
}
