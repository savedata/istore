import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of } from 'rxjs';

import { environment } from '../environments';
import { IStoreMessageInterface, IStoreUserInterface } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class IStoreUserApiService {

  constructor(private httpClient: HttpClient) { }

  private get apiPath(): string {
    return environment.backend;
  }

  private get contentPath(): string {
    return `${this.apiPath}/test`;
  }

  private get userPath(): string {
    return `${this.apiPath}/users`;
  }

  public getContent(path: string): Observable<IStoreMessageInterface> {
    return this.httpClient.get<IStoreMessageInterface>(`${this.contentPath}/${path}`);
  }

  public getUserInfo():Observable<IStoreUserInterface | null> {
    return this.httpClient
      .get<IStoreUserInterface>(this.userPath)
      .pipe(catchError(error => of(null)));
  }

  public updateUserInfo(user: Partial<IStoreUserInterface>): Observable<IStoreUserInterface> {
    return this.httpClient.patch<IStoreUserInterface>(this.userPath, user);
  }
}
