import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

const DEFAULT_SIDENAV = 'default';

@Injectable({ providedIn: 'root' })
export class IStoreSidenavService {
  private readonly sidenavList = new Map<string, BehaviorSubject<boolean>>();

  public switchSidenav(sidenavName = DEFAULT_SIDENAV, status: boolean | null = null): void {
    const previousStatus = this.sidenavList.get(sidenavName);
    const nextStatus = status !== null ? status : previousStatus ? !previousStatus.value : true;
    if (previousStatus) {
      previousStatus.next(nextStatus);
    } else {
      this.sidenavList.set(sidenavName, new BehaviorSubject<boolean>(nextStatus));
    }
  }

  public sidenavStatus(sidenavName = DEFAULT_SIDENAV): BehaviorSubject<boolean> {
    const previousStatus = this.sidenavList.get(sidenavName);

    return previousStatus ? previousStatus : new BehaviorSubject<boolean>(false);
  }
}
