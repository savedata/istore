import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { IStoreCategoryInterface, IStoreProductInterface } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class IStoreProductService {
  public readonly categoryList$ = new BehaviorSubject<IStoreCategoryInterface[]>([]);
  public readonly productList$ = new BehaviorSubject<IStoreProductInterface[]>([]);
}
