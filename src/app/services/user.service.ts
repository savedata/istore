import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { IStoreUserRoles } from '../enums';
import { IStoreUserInterface } from '../interfaces';

@Injectable({ providedIn: 'root' })
export class IStoreUserService {
  public readonly currentUser$ = new BehaviorSubject<IStoreUserInterface | null>(null);

  public get isAdmin(): boolean {
    return this.userRoles.includes(IStoreUserRoles.Admin);
  }

  public get isModerator(): boolean {
    return this.userRoles.includes(IStoreUserRoles.Moderator);
  }

  public get isUser(): boolean {
    return this.userRoles.includes(IStoreUserRoles.User);
  }

  public get userRoles(): IStoreUserRoles[] {
    return this.currentUser$.value ? this.currentUser$.value.roles as IStoreUserRoles[] : [];
  }
}
