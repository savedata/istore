import { IStoreRoutingPathsEnum } from './enums';
import { IStoreNavigationListInterface } from './interfaces';

export const NAVIGATION_LIST: IStoreNavigationListInterface = {
  [IStoreRoutingPathsEnum.Account]: {
    path: [IStoreRoutingPathsEnum.Account],
    title: 'Account',
  },
  [IStoreRoutingPathsEnum.Admin]: {
    path: [IStoreRoutingPathsEnum.Admin],
    title: 'Admin panel',
  },
  [IStoreRoutingPathsEnum.Catalog]: {
    path: [IStoreRoutingPathsEnum.Catalog],
    title: 'Catalog',
  },
  [IStoreRoutingPathsEnum.Contacts]: {
    path: [IStoreRoutingPathsEnum.Contacts],
    title: 'Contacts',
  },
  [IStoreRoutingPathsEnum.Auth]: {
    path: [IStoreRoutingPathsEnum.Auth],
    title: 'Auth',
  },
  [IStoreRoutingPathsEnum.SignIn]: {
    path: [IStoreRoutingPathsEnum.Auth, IStoreRoutingPathsEnum.SignIn],
    title: 'Sign In',
  },
  [IStoreRoutingPathsEnum.SignUp]: {
    path: [IStoreRoutingPathsEnum.Auth, IStoreRoutingPathsEnum.SignUp],
    title: 'Sign Up',
  },
};
