import { IStoreUserInterface } from '../interfaces';

export const USER_LIST_MOCK: IStoreUserInterface[] = [
  {
    accessToken: '',
    email: 'vitaly.durnykh@ex.com',
    firstName: 'Vitaly',
    lastName: 'Durnykh',
    password: '123',
    phoneNumber: '9232982385',
    roles: ['admin', 'moderator'], //для регистрации
  },
  {
    accessToken: '',
    email: 'kristina.tabunchik@ex.com',
    firstName: 'Kristina',
    lastName: 'Tabunchik',
    password: '123',
    phoneNumber: '9659054006',
    roles: ['moderator'], //для регистрации
  },
  {
    accessToken: '',
    email: 'denis.ostapenko@ex.com',
    firstName: 'Denis',
    lastName: 'Ostapenko',
    password: '123',
    phoneNumber: '9994403126',
    roles: ['user'], //для регистрации
  },
  {
    accessToken: '',
    email: 'jackie.chan@ex.com',
    firstName: 'Jackie',
    lastName: 'Chan',
    password: '123',
    phoneNumber: '9233381277',
    roles: ['user'], //для регистрации
  },
];
