import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router } from '@angular/router';
import { map, Observable, of, take, tap } from 'rxjs';

import { IStoreRoutingPathsEnum, IStoreUserRoles } from '../enums';
import { IStoreAuthService, IStoreUserApiService, IStoreUserService } from '../services';

@Injectable()
export class IStoreAccessGuard implements CanActivate, CanLoad {

  constructor(
    private router: Router,
    private iStoreAuthService: IStoreAuthService,
    private iStoreUserApiService: IStoreUserApiService,
    private iStoreUserService: IStoreUserService,
  ) { }

  private readonly guard = (route: ActivatedRouteSnapshot | Route): Observable<boolean> => {
    const acceptForUser: boolean = route.data ? route.data['acceptForUser'] : false;
    const checkAdminRole: boolean = route.data ? route.data['checkAdminRole'] : false;
    const redirectPath: IStoreRoutingPathsEnum = route.data
      ? route.data['customRedirect']
      : IStoreRoutingPathsEnum.Catalog;
    const checkUser$ = this.iStoreUserService.currentUser$.value
      ? this.iStoreUserService.currentUser$
      : this.iStoreAuthService.checkAccessToken()
        ? this.iStoreUserApiService.getUserInfo()
        : of(null);

    return checkUser$
        .pipe(
          take(1),
          map(user => acceptForUser
            ? !!user && checkAdminRole ? user.roles.includes(IStoreUserRoles.Admin) : !!user
            : !user,
          ),
          tap(isAllowAccess => {
            !isAllowAccess && this.router.navigate([redirectPath]);
          }));
  }

  canActivate = this.guard;
  canLoad = this.guard;
}
